<?php
/**
 * Catch-all template
 *
 * @package vamtam/consulting
 */

$format = get_query_var( 'post_format' );

VamtamFramework::set( 'page_title', $format ? sprintf( esc_html__( 'Post format: %s', 'vamtam-consulting' ), $format ) : esc_html__( 'Blog', 'vamtam-consulting' ) );

get_header();
?>
<div class="page-wrapper">

	<article <?php post_class( VamtamTemplates::get_layout() ) ?>>
		<div class="page-content clearfix">
			<?php get_template_part( 'loop', 'index' ); ?>
		</div>
	</article>

	<?php get_template_part( 'sidebar' ) ?>
</div>
<?php get_footer(); ?>
