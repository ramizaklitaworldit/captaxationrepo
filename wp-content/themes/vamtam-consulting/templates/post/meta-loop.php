<?php
/**
 * Post metadata template
 *
 * @package vamtam/consulting
 */

?>
<div class="post-meta">
	<nav class="clearfix">
		<?php get_template_part( 'templates/post/meta/categories' ); ?>
		<?php get_template_part( 'templates/post/meta/tags' ); ?>
	</nav>
</div>
