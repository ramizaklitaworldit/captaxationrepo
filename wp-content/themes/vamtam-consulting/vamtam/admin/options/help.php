<?php
return array(
	'name' => esc_html__( 'Help', 'vamtam-consulting' ),
	'auto' => true,
	'config' => array(

		array(
			'name' => esc_html__( 'Help', 'vamtam-consulting' ),
			'type' => 'title',
			'desc' => '',
		),

		array(
			'name' => esc_html__( 'Help', 'vamtam-consulting' ),
			'type' => 'start',
			'nosave' => true,
		),
//----
		array(
			'type' => 'docs',
		),

			array(
				'type' => 'end',
			),
	),
);
