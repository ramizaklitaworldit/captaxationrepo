<?php

return array(
	array(
		'label'     => esc_html__( 'Layout', 'vamtam-consulting' ),
		'id'        => 'top-bar-layout',
		'type'      => 'select',
		'transport' => 'postMessage',
		'choices'   => vamtam_get_beaver_layouts( array(
			''            => esc_html__( 'Disabled', 'vamtam-consulting' ),
			'menu-social' => esc_html__( 'Left: Menu, Right: Social Icons', 'vamtam-consulting' ),
			'social-menu' => esc_html__( 'Left: Social Icons, Right: Menu', 'vamtam-consulting' ),
			'text-menu'   => esc_html__( 'Left: Text, Right: Menu', 'vamtam-consulting' ),
			'menu-text'   => esc_html__( 'Left: Menu, Right: Text', 'vamtam-consulting' ),
			'social-text' => esc_html__( 'Left: Social Icons, Right: Text', 'vamtam-consulting' ),
			'text-social' => esc_html__( 'Left: Text, Right: Social Icons', 'vamtam-consulting' ),
			'fulltext'    => esc_html__( 'Text only', 'vamtam-consulting' ),
			'none'        => esc_html__( '--- Builder Templates: ---', 'vamtam-consulting' ),
		), 'beaver-' ),
	),

	array(
		'label'       => esc_html__( 'Text', 'vamtam-consulting' ),
		'description' => esc_html__( 'You can place plain text, HTML and shortcodes.', 'vamtam-consulting' ),
		'id'          => 'top-bar-text',
		'type'        => 'textarea',
		'transport'   => 'postMessage',
	),

	array(
		'label'     => esc_html__( 'Social Text Lead', 'vamtam-consulting' ),
		'id'        => 'top-bar-social-lead',
		'type'      => 'text',
		'transport' => 'postMessage',
	),

	array(
		'label'     => esc_html__( 'Facebook Link', 'vamtam-consulting' ),
		'id'        => 'top-bar-social-fb',
		'type'      => 'text',
		'transport' => 'postMessage',
	),

	array(
		'label'     => esc_html__( 'Twitter Link', 'vamtam-consulting' ),
		'id'        => 'top-bar-social-twitter',
		'type'      => 'text',
		'transport' => 'postMessage',
	),

	array(
		'label'     => esc_html__( 'LinkedIn Link', 'vamtam-consulting' ),
		'id'        => 'top-bar-social-linkedin',
		'type'      => 'text',
		'transport' => 'postMessage',
	),

	array(
		'label'     => esc_html__( 'Google+ Link', 'vamtam-consulting' ),
		'id'        => 'top-bar-social-gplus',
		'type'      => 'text',
		'transport' => 'postMessage',
	),

	array(
		'label'     => esc_html__( 'Flickr Link', 'vamtam-consulting' ),
		'id'        => 'top-bar-social-flickr',
		'type'      => 'text',
		'transport' => 'postMessage',
	),

	array(
		'label'     => esc_html__( 'Pinterest Link', 'vamtam-consulting' ),
		'id'        => 'top-bar-social-pinterest',
		'type'      => 'text',
		'transport' => 'postMessage',
	),

	array(
		'label'     => esc_html__( 'Dribbble Link', 'vamtam-consulting' ),
		'id'        => 'top-bar-social-dribbble',
		'type'      => 'text',
		'transport' => 'postMessage',
	),

	array(
		'label'     => esc_html__( 'Instagram Link', 'vamtam-consulting' ),
		'id'        => 'top-bar-social-instagram',
		'type'      => 'text',
		'transport' => 'postMessage',
	),

	array(
		'label'     => esc_html__( 'YouTube Link', 'vamtam-consulting' ),
		'id'        => 'top-bar-social-youtube',
		'type'      => 'text',
		'transport' => 'postMessage',
	),

	array(
		'label'     => esc_html__( 'Vimeo Link', 'vamtam-consulting' ),
		'id'        => 'top-bar-social-vimeo',
		'type'      => 'text',
		'transport' => 'postMessage',
	),

	array(
		'label'       => esc_html__( 'Background', 'vamtam-consulting' ),
		'description' => wp_kses( __( 'If you want to use an image as a background, enabling the cover button will resize and crop the image so that it will always fit the browser window on any resolution.<br> If the color opacity is less than 1 the page background underneath will be visible.', 'vamtam-consulting' ), [ 'br' => [] ] ),
		'id'          => 'top-nav-background',
		'type'        => 'background',
		'transport'   => 'postMessage',
		'show'        => array(
			'background-attachment' => false,
			'background-position'   => false,
		),
	),

	array(
		'label'   => esc_html__( 'Colors', 'vamtam-consulting' ),
		'type'    => 'color-row',
		'id'      => 'css-tophead',
		'choices' => array(
			'text-color'       => esc_html__( 'Text Color:', 'vamtam-consulting' ),
			'link-color'       => esc_html__( 'Link Color:', 'vamtam-consulting' ),
			'link-hover-color' => esc_html__( 'Link Hover Color:', 'vamtam-consulting' ),
		),
		'compiler'  => true,
		'transport' => 'postMessage',
	),

	array(
		'label'   => esc_html__( 'Sub-Menus', 'vamtam-consulting' ),
		'type'    => 'color-row',
		'id'      => 'submenu',
		'choices' => array(
			'background'  => esc_html__( 'Background:', 'vamtam-consulting' ),
			'color'       => esc_html__( 'Text Normal Color:', 'vamtam-consulting' ),
			'hover-color' => esc_html__( 'Text Hover Color:', 'vamtam-consulting' ),
		),
		'compiler'  => true,
		'transport' => 'postMessage',
	),
);
