<?php
/**
 * Theme options / Styles / General Typography
 *
 * @package vamtam/consulting
 */

return array(

array(
	'label'  => esc_html__( 'Headlines', 'vamtam-consulting' ),
	'type'   => 'heading',
	'id'     => 'styles-typography-headlines',
),

array(
	'label'      => esc_html__( 'H1', 'vamtam-consulting' ),
	'id'         => 'h1',
	'type'       => 'typography',
	'compiler'   => true,
	'transport'  => 'postMessage',
),

array(
	'label'      => esc_html__( 'H2', 'vamtam-consulting' ),
	'id'         => 'h2',
	'type'       => 'typography',
	'compiler'   => true,
	'transport'  => 'postMessage',
),

array(
	'label'      => esc_html__( 'H3', 'vamtam-consulting' ),
	'id'         => 'h3',
	'type'       => 'typography',
	'compiler'   => true,
	'transport'  => 'postMessage',
),

array(
	'label'      => esc_html__( 'H4', 'vamtam-consulting' ),
	'id'         => 'h4',
	'type'       => 'typography',
	'compiler'   => true,
	'transport'  => 'postMessage',
),

array(
	'label'      => esc_html__( 'H5', 'vamtam-consulting' ),
	'id'         => 'h5',
	'type'       => 'typography',
	'compiler'   => true,
	'transport'  => 'postMessage',
),

array(
	'label'      => esc_html__( 'H6', 'vamtam-consulting' ),
	'id'         => 'h6',
	'type'       => 'typography',
	'compiler'   => true,
	'transport'  => 'postMessage',
),

array(
	'label'  => esc_html__( 'Additional Fonts', 'vamtam-consulting' ),
	'type'   => 'heading',
	'id'     => 'styles-typography-additional',
),

array(
	'label'      => esc_html__( 'Emphasis Font', 'vamtam-consulting' ),
	'id'         => 'em',
	'type'       => 'typography',
	'compiler'   => true,
	'transport'  => 'postMessage',
),

array(
	'label'      => esc_html__( 'Style 1', 'vamtam-consulting' ),
	'id'         => 'additional-font-1',
	'type'       => 'typography',
	'compiler'   => true,
	'transport'  => 'postMessage',
),

array(
	'label'      => esc_html__( 'Style 2', 'vamtam-consulting' ),
	'id'         => 'additional-font-2',
	'type'       => 'typography',
	'compiler'   => true,
	'transport'  => 'postMessage',
),

array(
	'label'  => esc_html__( 'Google Fonts Options', 'vamtam-consulting' ),
	'type'   => 'heading',
	'id'     => 'styles-typography-gfonts',
),

array(
	'label'      => esc_html__( 'Subsets', 'vamtam-consulting' ),
	'id'         => 'gfont-subsets',
	'type'       => 'multicheck',
	'transport'  => 'postMessage',
	'choices'    => vamtam_get_google_fonts_subsets(),
),

);
