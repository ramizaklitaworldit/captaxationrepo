<?php

/**
 * Theme options / Layout / General
 *
 * @package vamtam/consulting
 */

return array(

	array(
		'label'       => esc_html__( 'Layout Type', 'vamtam-consulting' ),
		'description' => esc_html__( 'Please note that in full width layout mode, the body background option found in Styles - Body, acts as page background.', 'vamtam-consulting' ),
		'id'          => 'site-layout-type',
		'type'        => 'radio',
		'choices'     => array(
			'boxed' => esc_html__( 'Boxed', 'vamtam-consulting' ),
			'full'  => esc_html__( 'Full width', 'vamtam-consulting' ),
		),
	),

	array(
		'label'       => esc_html__( 'Boxed Layout Padding', 'vamtam-consulting' ),
		'description' => esc_html__( 'Add padding between the edge of the box and the page content. Only used on VamTam Builder pages.', 'vamtam-consulting' ),
		'id'          => 'boxed-layout-padding',
		'type'        => 'switch',
	),

	array(
		'label'       => esc_html__( 'Maximum Page Width', 'vamtam-consulting' ),
		'description' => wp_kses( sprintf( __( 'If you have changed this option, please use the <a href="%s" title="Regenerate thumbnails" target="_blank">Regenerate thumbnails</a> plugin in order to update your images.', 'vamtam-consulting' ), 'http://wordpress.org/extend/plugins/regenerate-thumbnails/' ), 'vamtam-a-span' ),
		'id'          => 'site-max-width',
		'type'        => 'radio',
		'choices'     => array(
			1140 => '1140px',
			1260 => '1260px',
			1400 => '1400px',
		),
		'compiler'  => true,
		'transport' => 'postMessage',
	),

);
