<?php

/**
 * Theme options / General / Media
 *
 * @package vamtam/consulting
 */


return array(
	array(
		'label'       => esc_html__( 'Custom JavaScript', 'vamtam-consulting' ),
		'description' => wp_kses( __( 'If the hundreds of options in the Theme Options Panel are not enough and you need customisation that is outside of the scope of the Theme Option Panel please place your javascript in this field. The contents of this field are placed near the <code>&lt;/body&gt;</code> tag, which improves the load times of the page.', 'vamtam-consulting' ), [ 'code' => '' ] ),
		'id'          => 'custom-js',
		'type'        => 'textarea',
	),
);
