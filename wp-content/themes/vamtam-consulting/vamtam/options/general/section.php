<?php

global $vamtam_theme_customizer;

$thispath = VAMTAM_OPTIONS . 'general/';

$vamtam_theme_customizer->add_section( array(
	'title'       => esc_html__( 'General', 'vamtam-consulting' ),
	'description' => '',
	'id'          => 'general',
) );

$vamtam_theme_customizer->add_section( array(
	'title'       => esc_html__( 'General', 'vamtam-consulting' ),
	'description' => '',
	'id'          => 'general-general',
	'subsection'  => true,
	'fields'      => include $thispath . 'general.php',
) );

$vamtam_theme_customizer->add_section( array(
	'title'       => esc_html__( 'Posts', 'vamtam-consulting' ),
	'description' => '',
	'id'          => 'general-posts',
	'subsection'  => true,
	'fields'      => include $thispath . 'posts.php',
) );

$vamtam_theme_customizer->add_section( array(
	'title'       => esc_html__( 'Projects', 'vamtam-consulting' ),
	'description' => '',
	'id'          => 'general-projects',
	'subsection'  => true,
	'fields'      => include $thispath . 'projects.php',
) );
