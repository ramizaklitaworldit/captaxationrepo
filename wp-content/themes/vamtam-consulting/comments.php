<?php
/**
 * Comments template
 *
 * @package vamtam/consulting
 */

if ( is_page_template( 'page-blank.php' ) ) {
	return;
}

wp_reset_postdata();

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Please do not load this page directly. Thanks!' );
}

$req = get_option( 'require_name_email' ); // Checks if fields are required.

// cookies consent
$commenter = wp_get_current_commenter();
$consent   = empty( $commenter['comment_author_email'] ) ? '' : ' checked="checked"';

// do not display anything if the post is protected or the comments are closed and there is no comment history
if (
	( ! empty( $post->post_password ) && post_password_required() ) ||
	( ! comments_open() && ! have_comments() ) ||
	! post_type_supports( get_post_type(), 'comments' ) ) {
	return;
}

?>
<div class="limit-wrapper clearboth">
	<div id="comments" class="comments-wrapper">
		<?php if ( have_comments() ) : ?>
			<?php // numbers of pings and comments
			$ping_count = $comment_count = 0;
			foreach ( $comments as $comment ) {
				get_comment_type() == 'comment' ? ++$comment_count : ++$ping_count;
			}
			?>

			<div class="sep-text centered keep-always">
				<div class="content">
					<?php comments_popup_link( esc_html__( '0 Comments:', 'vamtam-consulting' ), esc_html__( '1 Comment', 'vamtam-consulting' ), esc_html__( '% Comments:', 'vamtam-consulting' ) ); ?>
				</div>
			</div>

			<?php if ( $comment_count ) : ?>
				<div id="comments-list" class="comments">
					<?php wp_list_comments( array(
						'type'     => 'comment',
						'callback' => array( 'VamtamTemplates', 'comments' ),
						'style'    => 'div',
					) ); ?>
				</div>
			<?php endif; ?>

			<?php if ( $ping_count ) : ?>
				<div class="sep-text centered keep-always">
					<div class="content">
						<?php echo sprintf( $ping_count > 1 ? esc_html__( '%d Trackbacks:', 'vamtam-consulting' ) : esc_html__( 'One Trackback:', 'vamtam-consulting' ), (int) (int) $ping_count );  // xss ok ?>
					</div>
				</div>
				<div id="trackbacks-list" class="comments">
					<?php wp_list_comments( array(
						'type'       => 'pings',
						'callback'   => array( 'VamtamTemplates', 'comments' ),
						'style'      => 'div',
						'short_ping' => true,
					) ); ?>
				</div>
			<?php endif ?>
		<?php endif ?>

		<?php
			the_comments_pagination( array(
				'prev_text' => '<span class="screen-reader-text">' . esc_html__( 'Previous', 'vamtam-consulting' ) . '</span>',
				'next_text' => '<span class="screen-reader-text">' . esc_html__( 'Next', 'vamtam-consulting' ) . '</span>',
			) );
		?>

		<div class="respond-box">
			<?php if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>
				<p class="no-comments"><?php esc_html_e( 'Comments are closed.', 'vamtam-consulting' ); ?></p>
			<?php endif; ?>
			<?php if ( get_option( 'comment_registration' ) && ! $user_ID ) : ?>
				<p id="login-req"><?php printf( wp_kses( __( 'You must be <a href="%s" title="Log in">logged in</a> to post a comment.', 'vamtam-consulting' ), 'vamtam-a-span' ), esc_url( get_option( 'siteurl' ) . '/wp-login.php?redirect_to=' . get_permalink() ) ) ?></p>
			<?php else : ?>
				<?php comment_form(); ?>
			<?php endif /* if ( get_option( 'comment_registration' ) && !$user_ID ) */ ?>
		</div><!-- .respond-box -->
	</div><!-- #comments -->
</div>
