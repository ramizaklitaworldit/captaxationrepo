<?php
//include('shortcodes.php');
require_once(get_stylesheet_directory().'/widgets/wpb_widget.php');
require_once(get_stylesheet_directory().'/widgets/wpb_certificates_widgets.php');
require_once(get_stylesheet_directory().'/widgets/wpb_why_us_widgets.php');
require_once(get_stylesheet_directory().'/widgets/wpb_team_members_widgets.php');
function child_styles() {
	wp_enqueue_style( 'my-child-theme-style', get_stylesheet_directory_uri() . '/style.css', array( 'front-all' ), false, 'all' );
}
add_action( 'wp_enqueue_scripts', 'child_styles', 11 );