<div class="why-us-block">
	<?php $rows = 4; ?>
	<?php $i=0;foreach($posts as $pt) {$i++;?>
		<?php 
			$post_title = $pt->post_title;
			$brief = $pt->post_content;
			$image = get_the_post_thumbnail_url($pt->ID);
		?>
		<div class="why-us-item">
			<div class="why-us-icon">
				<?php if(!empty($image)) {?>
					<img src="<?php echo $image; ?>" alt="<?php echo $post_title; ?>" title="<?php echo $post_title; ?>" />
				<?php }?>
			</div>
			<div class="why-us-title"><h5 class="text-center"><?php echo $post_title; ?></h5></div>
			<!-- <div class="why-us-brief"><?php //echo $brief; ?></div> -->
		</div>
	<?php }?>
</div>