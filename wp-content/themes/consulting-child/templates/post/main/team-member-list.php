<div class="team-members-custom-listing">
	<?php foreach($posts as $post) {
		$post_id = $post->ID;
		$post_title = $post->post_title;
		$post_meta = get_post_meta($post_id);
		//print '<pre>'; print_r($post);
		//print '<pre>'; print_r($post_meta);
		$permalink = get_post_permalink($post_id);
		$brief = $post->post_excerpt;
		$icons_map = array(
			'googleplus' => 'vamtam-theme-google-plus',
			'linkedin'   => 'vamtam-theme-linkedin',
			'facebook'   => 'vamtam-theme-facebook',
			'twitter'    => 'vamtam-theme-twitter',
			'youtube'    => 'vamtam-theme-youtube',
			'pinterest'  => 'vamtam-theme-pinterest',
			'lastfm'     => 'vamtam-theme-last-fm',
			'instagram'  => 'vamtam-theme-instagram',
			'dribble'    => 'vamtam-theme-dribbble',
			'vimeo'      => 'vamtam-theme-vimeo',
		);
		$used_icons = array();
		foreach ( $icons_map as $icon => $icon_name ) {
			if ( isset( $post_meta[$icon][0] ) && ! empty( $post_meta[$icon][0] ) ) {
				$used_icons[] = array(
					'url'  => esc_url($post_meta[$icon][0]),
					'name' => $icon_name,
				);
			}
		}
		?>
		<div class="team-member-custom-item">	
			<div class="team-member <?php echo ( ! empty( $brief ) ? 'has-content' : '' ) ?>">
				<?php if ( isset($post_meta['_thumbnail_id'][0]) && ! empty( $post_meta['_thumbnail_id'][0] ) ) :  ?>
					<div class="article-type-team ">
						<div class="thumbnail">
							<?php if ( ! empty( $permalink ) ) : ?>
								<a href="<?php echo esc_url( $permalink ) ?>" title="<?php echo esc_attr( $post_title ) ?>">
							<?php endif ?>
								<?php echo VamtamTemplates::lazyload_image( $post_meta['_thumbnail_id'][0], 'full' ); ?>
							<?php if ( ! empty( $permalink ) ) : ?>
								</a>
							<?php endif ?>

							<?php if ( ! empty( $used_icons ) ): ?>
								<div class="share-icons clearfix">
									<?php foreach ( $used_icons as $icon ) : ?>
										<a target="_blank" href="<?php echo esc_url( $icon['url'] )?>"><?php echo vamtam_get_icon_html( array( // xss ok
											'name' => $icon['name'],
										) ); ?></a>
									<?php endforeach; ?>
								</div>
							<?php endif ?>
						</div>
					</div>
				<?php endif ?>
				<div class="team-member-info">

					<?php if(isset($post_meta['position'][0]) && !empty($post_meta['position'][0])) {?>
						<h5 class="regular-title-wrapper team-member-position"><?php echo $post_meta['position'][0]; ?></h5>
					<?php }?>

					<h4>
						<?php if ( ! empty( $permalink ) ) : ?>
							<a href="<?php echo esc_url( $permalink ) ?>" title="<?php echo esc_attr( $post_title ) ?>">
						<?php endif ?>
							<?php echo wp_kses_post( $post_title ) ?>
						<?php if ( ! empty( $permalink ) ) : ?>
							</a>
						<?php endif ?>
					</h4>

					<!-- <?php //if ( ! empty( $settings->phone ) ) : ?>
						<div class="team-member-phone"><a href="tel:<?php //echo esc_attr( $settings->phone ) ?>" title="<?php //echo esc_attr( sprintf( 'Call %s', $settings->name ) ) ?>"><?php //esc_html_e( 'Tel:', 'vamtam-consulting' ) ?> <?php //echo wp_kses_post( $settings->phone ) ?></a></div>
					<?php //endif ?>
					<?php //if ( ! empty( $settings->email ) ) : ?>
						<div  class="team-member-email"><a href="mailto:<?php //echo esc_attr( $settings->email ) ?>" title="<?php //echo esc_attr( sprintf( __( 'email %s', 'vamtam-consulting' ), $settings->name ) ) ?>"><?php //echo wp_kses_post( $settings->email ); ?></a></div>
					<?php //endif ?> -->

					<?php if ( ! empty( $brief ) ) :  ?>
						<div class="team-member-bio">
							<?php echo do_shortcode( $brief ) ?>
						</div>
					<?php endif ?>
				</div>
			</div>
		</div>
	<?php }?>
</div>