<div class="our-journey-block">
	<?php $rows = 4; ?>
	<div class="our-journey-row">
		<?php $i=0;foreach($posts as $pt) {$i++;?>
			<?php 
			 $post_title = $pt->post_title;
			 $brief = $pt->post_excerpt;
			 //$journey_icon = get_post_meta($pt->ID,'journey_icon',true);

			$journey_icon_id = get_post_meta($pt->ID,'icon',true);
			$content_post = get_post($journey_icon_id);
			$journey_icon = $content_post->guid;


			?>
			<div class="our-journey-item even w-<?php echo $rows; ?>">
				<?php if($journey_icon != '') {?>
					<div class="our-journey-image">
						<img src="<?php echo $journey_icon; ?>" alt="<?php  echo $post_title; ?>" title="<?php  echo $post_title; ?>" />
					</div>
				<?php }?>
				<h5 class="our-journey-title">
					<?php echo $post_title; ?>
				</h5>
				<div class="our-journey-brief">
					<?php echo $brief; ?>
				</div>
			</div>
			<?php if($i%$rows == 0 && $i != count($posts)) {?></div><div class="our-journey-row"><?php }?>
		<?php }?>
	</div>
</div>