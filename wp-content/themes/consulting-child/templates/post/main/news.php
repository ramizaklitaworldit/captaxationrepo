<?php 
$post_id = $post->ID;
$post_type = $post->post_type;
if ( $show_media ) :  ?>
	<?php 
		//print '<pre>'; print_r($post); 
		
		
		$services_icon_id = get_post_meta($post_id,'service-icon',true);
		$content_post = get_post($services_icon_id);
		$service_icon = $content_post->guid;
	?>
	<div class="post-media article-type-<?php echo $post_type; ?>">
		<?php if($post_type == 'services') {?>
			<div class="thumbnail services-icon">
				<a href="<?php the_permalink() ?>" title="<?php the_title_attribute()?>">
					<img src="<?php echo $service_icon; ?>" alt="<?php the_title_attribute()?>" title="<?php the_title_attribute()?>" />
				</a>
			</div>
		<?php } else {?>
			<?php if ( isset( $post_data['media'] ) ) :  ?>
				<div class="thumbnail">
					<?php if ( has_post_format( 'image' ) || ( isset( $post_data['act_as_image'] ) && $post_data['act_as_image'] ) ) :  ?>
						<a href="<?php the_permalink() ?>" title="<?php the_title_attribute()?>">
							<?php echo $post_data['media']; // xss ok ?>
							<?php echo vamtam_get_icon_html( array( // xss ok
								'name' => 'vamtam-theme-circle-post',
							) ); ?>
						</a>
					<?php else : ?>
						<?php echo $post_data['media']; // xss ok ?>
					<?php endif ?>
				</div>
			<?php endif; ?>
		<?php }?>
	</div>
<?php endif ?>

<?php if ( $show_content || $show_title ) :  ?>
	<div class="post-content-wrapper">
		<?php
			$show_tax = vamtam_get_optionb( 'post-meta', 'tax' ) && ( ! post_password_required() || is_customize_preview() );
			$tags     = get_the_tags();
		?>

		<?php
			$categories_list = get_the_category_list( ', ' );
			if ( $categories_list && ( $show_tax || is_customize_preview() ) ) :
		?>
			<div class="post-content-meta">
				<?php get_template_part( 'templates/post/meta/categories' ) ?>
			</div>
		<?php endif ?>

		<?php if ( $show_title ) : ?>
			<?php include locate_template( 'templates/post/header.php' ); ?>
		<?php endif ?>

		<?php get_template_part( 'templates/post/main/actions' ) ?>

		<?php if ( $show_content ) : 
			$post_meta = get_post_meta($post_id);

			?>

			<div class="post-content-outer">
				<?php if(isset($post_meta['position'][0]) && !empty($post_meta['position'][0])) {?>
					<div class="team-position"><?php echo $post_meta['position'][0]; ?></div>
				<?php }?>
				<?php echo $post_data['content']; // xss ok ?>
				<?php if($post_type == 'team') {//print '<pre>'; print_r($post_meta);?>

				<div class="team-social-media">

					<?php if(isset($post_meta['facebook'][0]) && !empty($post_meta['facebook'][0])) {?>
						<a href="<?php echo $post_meta['facebook'][0]; ?>"><i class="fab fa-facebook-f"></i></a>
					<?php }?>

					<?php if(isset($post_meta['twitter'][0]) && !empty($post_meta['twitter'][0])) {?>
						<a href="<?php echo $post_meta['twitter'][0]; ?>"><i class="fab fa-twitter"></i></a>
					<?php }?>

					<?php if(isset($post_meta['instagram'][0]) && !empty($post_meta['instagram'][0])) {?>
						<a href="<?php echo $post_meta['facebook'][0]; ?>"><i class="fab fa-instagram"></i></a>
					<?php }?>

					<?php if(isset($post_meta['linkedin'][0]) && !empty($post_meta['linkedin'][0])) {?>
						<a href="<?php echo $post_meta['linkedin'][0]; ?>"><i class="fab fa-linkedin-in"></i></a>
					<?php }?>
					
				</div>
			<?php }?>
			</div>
			
		<?php endif ?>

		<?php if ( ( $show_tax || is_customize_preview() ) && !!$tags ) : ?>
			<div class="post-content-meta">
				<div class="the-tags vamtam-meta-tax" <?php VamtamTemplates::display_none( $show_tax ) ?>>
					<?php the_tags( '<span class="icon">' . vamtam_get_icon( 'tag' ) . '</span> <span class="visuallyhidden">' . esc_html__( 'Tags', 'vamtam-consulting' ) . '</span> ', ', ', '' ); ?>
				</div>
			</div>
		<?php endif ?>

		<div class="vamtam-button-wrap vamtam-button-width-auto vamtam-button-left">
			<a href="<?php the_permalink() ?>" target="_self" class="vamtam-button accent3 hover-accent1 button-underline" role="button">
				<span class="vamtam-button-text"><?php esc_html_e( 'Read More', 'vamtam-consulting' ) ?></span>
			</a>
		</div>



	</div>
<?php endif; ?>
