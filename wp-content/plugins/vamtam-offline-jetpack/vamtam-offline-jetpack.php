<?php

/*
Plugin Name: VamTam Offline Jetpack
Description: Switches Jetpack to Offline Mode when active
Version: 1.0.0
Author: VamTam
Author URI: http://vamtam.com
*/


if ( ! class_exists( 'Vamtam_Updates_2' ) ) {
	require 'vamtam-updates/class-vamtam-updates.php';
}

new Vamtam_Updates_2( __FILE__ );

add_action( 'admin_init', 'vamtam_offline_jetpack_admin_init' );

function vamtam_offline_jetpack_admin_init() {
	update_option( 'active_plugins', array_diff( get_option( 'active_plugins' ), [ 'unplug-jetpack/unplug-jetpack.php' ] ) );
}

add_action( 'plugins_loaded', 'vamtam_offline_jetpack_setup' );

function vamtam_offline_jetpack_setup() {
	add_filter( 'jetpack_offline_mode', '__return_true' );
}
