<?php

add_filter( 'wpml_beaver_builder_modules_to_translate', 'vamtam_wpml_beaver_builder_modules_to_translate' );
function vamtam_wpml_beaver_builder_modules_to_translate( $modules ) {
	if ( ! class_exists( 'FLBuilderModel' ) ) {
		return $modules;
	}

	foreach ( FLBuilderModel::$modules as $slug => $module ) {
		if ( strpos( $slug, 'vamtam-' ) === 0 ) {
			$fields = [];

			foreach ( $module->form as $tab_id => $tab ) {
				if ( isset( $tab['sections'] ) ) {
					foreach ( $tab['sections'] as $section_id => $section ) {
						foreach( $section['fields'] as $field_id => $field ) {
							if ( isset( $field['vamtam-wpml'] ) ) {
								$fields[] = [
									'field'       => $field_id,
									'type'        => $field['label'],
									'editor_type' => $field['vamtam-wpml'],
								];
							} elseif ( $field['type'] === 'editor' ) {
								$fields[] = [
									'field'       => $field_id,
									'type'        => $field['label'],
									'editor_type' => 'VISUAL',
								];
							} elseif ( $field['type'] === 'link' ) {
								$fields[] = [
									'field'       => $field_id,
									'type'        => $field['label'],
									'editor_type' => 'LINE',
								];
							}
						}
					}
				}
			}

			if ( ! empty( $fields ) ) {
				$modules[ $slug ] = [
					'conditions' => [ 'type' => $slug ],
					'fields' => $fields,
				];
			}

		}
	}

	if ( ! isset( $modules['vamtam-accordion'] ) ) {
		$modules['vamtam-accordion'] = [
			'conditions' => [ 'type' => 'vamtam-accordion' ],
			'fields'     => '',
		];
	}

	$modules['vamtam-accordion']['integration-class'] = 'Vamtam_WPML_Beaver_Builder_Accordion';

	if ( ! isset( $modules['vamtam-tabs'] ) ) {
		$modules['vamtam-tabs'] = [
			'conditions' => [ 'type' => 'vamtam-tabs' ],
			'fields'     => '',
		];
	}

	$modules['vamtam-tabs']['integration-class'] = 'Vamtam_WPML_Beaver_Builder_Tabs';

	if ( ! isset( $modules['vamtam-pricing-table'] ) ) {
		$modules['vamtam-pricing-table'] = [
			'conditions' => [ 'type' => 'vamtam-pricing-table' ],
			'fields'     => '',
		];
	}

	$modules['vamtam-pricing-table']['integration-class'] = 'Vamtam_WPML_Beaver_Builder_Pricing_Table';

	if ( ! isset( $modules['vamtam-icon-group'] ) ) {
		$modules['vamtam-icon-group'] = [
			'conditions' => [ 'type' => 'vamtam-icon-group' ],
			'fields'     => '',
		];
	}

	$modules['vamtam-icon-group']['integration-class'] = 'Vamtam_WPML_Beaver_Builder_Icon_Group';


	return $modules;
}

if ( class_exists( 'WPML_Beaver_Builder_Module_With_Items' ) ) {
	class Vamtam_WPML_Beaver_Builder_Accordion extends WPML_Beaver_Builder_Module_With_Items {
		protected function get_title( $field ) {
			switch( $field ) {
				case 'label':
					return esc_html__( 'Accordion Item Label', 'vamtam-elements-b' );

				case 'content':
					return esc_html__( 'Accordion Item Content', 'vamtam-elements-b' );

				default:
					return '';
			}
		}
	}

	class Vamtam_WPML_Beaver_Builder_Tabs extends WPML_Beaver_Builder_Module_With_Items {
		protected function get_title( $field ) {
			switch( $field ) {
				case 'label':
					return esc_html__( 'Tab Label', 'vamtam-elements-b' );

				case 'content':
					return esc_html__( 'Tab Content', 'vamtam-elements-b' );

				default:
					return '';
			}
		}
	}

	class Vamtam_WPML_Beaver_Builder_Pricing_Table extends WPML_Beaver_Builder_Module_With_Items {

		public function &get_items( $settings ) {
			return $settings->pricing_columns;
		}

		public function get_fields() {
			return array( 'title', 'button_text', 'button_url', 'features', 'price', 'duration' );
		}

		protected function get_title( $field ) {
			switch ( $field ) {
				case 'title':
					return esc_html__( 'Pricing table: Title', 'vamtam-elements-b' );

				case 'button_text':
					return esc_html__( 'Pricing table: Button text', 'vamtam-elements-b' );

				case 'button_url':
					return esc_html__( 'Pricing table: Button link', 'vamtam-elements-b' );

				case 'btn_icon':
					return esc_html__( 'Pricing table: Button icon', 'vamtam-elements-b' );

				case 'features':
					return esc_html__( 'Pricing table: Feature', 'vamtam-elements-b' );

				case 'price':
					return esc_html__( 'Pricing table: Price', 'vamtam-elements-b' );

				case 'duration':
					return esc_html__( 'Pricing table: Duration', 'vamtam-elements-b' );

				default:
					return '';

			}
		}

		protected function get_editor_type( $field ) {
			switch ( $field ) {
				case 'button_url':
					return 'LINK';

				case 'features':
					return 'VISUAL';

				case 'title':
				case 'button_text':
				case 'price':
				case 'duration':
				default:
					return 'LINE';
			}
		}
	}

	class Vamtam_WPML_Beaver_Builder_Icon_Group extends WPML_Beaver_Builder_Module_With_Items {

		public function &get_items( $settings ) {
			return $settings->icons;
		}

		public function get_fields() {
			return array( 'link' );
		}

		protected function get_title( $field ) {
			switch ( $field ) {
				case 'link':
					return esc_html__( 'Icon link', 'vamtam-elements-b' );

				default:
					return '';

			}
		}

		protected function get_editor_type( $field ) {
			switch ( $field ) {
				case 'link':
					return 'LINK';

				default:
					return '';
			}
		}

	}
}
