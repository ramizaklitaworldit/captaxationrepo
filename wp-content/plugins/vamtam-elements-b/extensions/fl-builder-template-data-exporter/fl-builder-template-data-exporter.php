<?php

// Defines
define( 'VAMTAMEL_B_TEMPLATE_DATA_EXPORTER_DIR', VAMTAMEL_B_DIR . 'extensions/fl-builder-template-data-exporter/' );
define( 'VAMTAMEL_B_TEMPLATE_DATA_EXPORTER_URL', VAMTAMEL_B_URL . 'extensions/fl-builder-template-data-exporter/' );

// Classes
require_once VAMTAMEL_B_TEMPLATE_DATA_EXPORTER_DIR . 'classes/class-fl-builder-template-data-exporter.php';
