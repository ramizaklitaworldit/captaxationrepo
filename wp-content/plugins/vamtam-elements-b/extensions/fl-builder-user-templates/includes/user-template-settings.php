<?php

FLBuilder::register_settings_form('user_template', array(
	'title' => __( 'Save Template', 'vamtam-elements-b' ),
	'tabs'  => array(
		'general' => array(
			'title'       => __( 'General', 'vamtam-elements-b' ),
			'description' => __( 'Save the current layout as a template that can be reused under <strong>Templates &rarr; Saved Templates</strong>.', 'vamtam-elements-b' ),
			'sections'    => array(
				'general' => array(
					'title'  => '',
					'fields' => array(
						'name' => array(
							'type'  => 'text',
							'label' => _x( 'Name', 'Template name.', 'vamtam-elements-b' ),
						),
					),
				),
			),
		),
	),
));
