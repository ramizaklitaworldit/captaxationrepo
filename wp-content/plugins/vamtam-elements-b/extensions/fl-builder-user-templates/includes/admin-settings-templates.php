<?php

$enabled_templates = FLBuilderModel::get_enabled_templates();

?>
<div id="fl-templates-form" class="fl-settings-form">

	<h3 class="fl-settings-form-header"><?php _e( 'Template Settings', 'vamtam-elements-b' ); ?></h3>

	<form id="templates-form" action="<?php FLBuilderAdminSettings::render_form_action( 'templates' ); ?>" method="post">

		<?php if ( FLBuilderAdminSettings::multisite_support() && ! is_network_admin() ) : ?>
		<label>
			<input class="fl-override-ms-cb" type="checkbox" name="fl-override-ms" value="1" <?php echo ( get_option( '_fl_builder_enabled_templates' ) ) ? 'checked="checked"' : ''; ?> />
			<?php _e( 'Override network settings?', 'vamtam-elements-b' ); ?>
		</label>
		<?php endif; ?>

		<div class="fl-settings-form-content">

			<h4><?php _e( 'Enable Templates', 'vamtam-elements-b' ); ?></h4>
			<p><?php _e( 'Use this setting to enable or disable templates in the builder interface.', 'vamtam-elements-b' ); ?></p>
			<select name="fl-template-settings">
				<option value="enabled" <?php selected( $enabled_templates, 'enabled' ); ?>><?php _e( 'Enable All Templates', 'vamtam-elements-b' ); ?></option>
				<option value="core" <?php selected( $enabled_templates, 'core' ); ?>><?php _e( 'Enable Core Templates Only', 'vamtam-elements-b' ); ?></option>
				<option value="user" <?php selected( $enabled_templates, 'user' ); ?>><?php _e( 'Enable User Templates Only', 'vamtam-elements-b' ); ?></option>
				<option value="disabled" <?php selected( $enabled_templates, 'disabled' ); ?>><?php _e( 'Disable All Templates', 'vamtam-elements-b' ); ?></option>
			</select>
			<?php do_action( 'fl_builder_admin_settings_templates_form' ); ?>
		</div>
		<p class="submit">
			<input type="submit" name="update" class="button-primary" value="<?php esc_attr_e( 'Save Template Settings', 'vamtam-elements-b' ); ?>" />
			<?php wp_nonce_field( 'templates', 'fl-templates-nonce' ); ?>
		</p>
	</form>
</div>
