<#
	let classes = [ 'text' ];

	if ( data.field.class ) {
		classes.push( data.field.class );
	}

	if ( data.field.size ) {
		classes.push( 'text-full' );
	}
#>
<input
	type="number"
	name="{{ data.field.name }}"
	value="{{ data.field.value }}"
	class="{{ classes.join( ' ' ) }}"
	<# if ( data.field.placeholder ) { #>placeholder="{{ data.field.placeholder }}"<# } #>
	<# if ( data.field.maxlength ) { #>maxlength="{{ data.field.maxlength }}"<# } #>
	<# if ( data.field.size ) { #>size="{{ data.field.size }}"<# } #>
	<# if ( data.field.min ) { #>min="{{ data.field.min }}"<# } #>
	<# if ( data.field.max ) { #>max="{{ data.field.max }}"<# } #>
	<# if ( data.field.step ) { #>step="{{ data.field.step }}"<# } #>
/>
