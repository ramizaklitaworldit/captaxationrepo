<?php

define( 'VAMTAMAC_B_DIR', plugin_dir_path( __FILE__ ) );
define( 'VAMTAMAC_B_URL', plugins_url( '/', __FILE__ ) );

class Vamtam_Additional_Content_B {

	// actions and filters
	public static function setup() {
		include 'classes/metaboxes-generator.php';

		add_action( 'add_meta_boxes', [ __CLASS__, 'load_metaboxes' ] );
		add_action( 'save_post', [ __CLASS__, 'load_metaboxes' ] );
	}

	/**
	 * Theme metaboxes
	 *
	 * @param int|null $post_id  id of the current post ( if any )
	 */
	public static function load_metaboxes( $post_id = null ) {
		$options = include VAMTAMAC_B_DIR . 'config/testimonials.php';
		new Vamtam_Additional_Content_B_Metaboxes_Generator( [
			'id'       => 'testimonials-post-options',
			'title'    => esc_html__( 'VamTam Testimonials', 'wpv' ),
			'pages'    => array( 'jetpack-testimonial' ),
			'context'  => 'normal',
			'priority' => 'high',
			'post_id'  => $post_id,
		], $options );

		$options = include VAMTAMAC_B_DIR . 'config/post-formats.php';
		new Vamtam_Additional_Content_B_Metaboxes_Generator( [
			'id'       => 'vamtam-post-format-options',
			'title'    => esc_html__( 'VamTam Post Formats', 'wpv' ),
			'pages'    => array( 'post' ),
			'context'  => 'normal',
			'priority' => 'high',
			'post_id'  => $post_id,
		], $options );

		$options = include VAMTAMAC_B_DIR . 'config/portfolio-formats.php';
		new Vamtam_Additional_Content_B_Metaboxes_Generator( [
			'id'       => 'vamtam-portfolio-format-options',
			'title'    => esc_html__( 'Project Formats', 'wpv' ),
			'pages'    => array( 'jetpack-portfolio' ),
			'context'  => 'normal',
			'priority' => 'high',
			'post_id'  => $post_id,
		], $options );

		$options = include VAMTAMAC_B_DIR . 'config/portfolio-formats-select.php';
		new Vamtam_Additional_Content_B_Metaboxes_Generator( [
			'id'       => 'vamtam-portfolio-formats-select',
			'title'    => esc_html__( 'Project Format', 'wpv' ),
			'pages'    => array( 'jetpack-portfolio' ),
			'context'  => 'side',
			'priority' => 'high',
			'post_id'  => $post_id,
		], $options );

		$options = include VAMTAMAC_B_DIR . 'config/general.php';
		new Vamtam_Additional_Content_B_Metaboxes_Generator( [
			'id'       => 'general-post-options',
			'title'    => esc_html__( 'VamTam Options', 'wpv' ),
			'pages'    => class_exists( 'VamtamFramework' ) ? VamtamFramework::$complex_layout : [ 'page', 'post', 'jetpack-portfolio', 'product' ],
			'context'  => 'normal',
			'priority' => 'high',
			'post_id'  => $post_id,
		], $options );
	}
}

Vamtam_Additional_Content_B::setup();