<?php

/**
 * Helper class for builder extensions.
 *
 * @since 1.0
 */
final class Vamtam_Elements_B_Extensions {

	/**
	 * Initalizes any extensions found in the extensions directory.
	 *
	 * @since 1.8
	 * @return void
	 */
	static public function init() {

		$extensions = glob( VAMTAMEL_B_DIR . 'extensions/*', GLOB_ONLYDIR );

		if ( ! is_array( $extensions ) ) {
			return;
		}

		foreach ( $extensions as $extension ) {
			$path = trailingslashit( $extension ) . basename( $extension ) . '.php';

			if ( file_exists( $path ) ) {
				require_once $path;
			}
		}
	}
}

Vamtam_Elements_B_Extensions::init();
