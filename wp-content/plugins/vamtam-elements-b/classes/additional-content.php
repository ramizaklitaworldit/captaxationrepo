<?php

class Vamtam_Elements_B_Additional_Content {
	public function __construct() {
		self::register_meta();

		add_action( 'add_meta_boxes', [ __CLASS__, 'add_meta_boxes' ] );
	}

	public static function register_meta() {
		register_meta( 'post', 'page-middle-header-type', [
			'type'         => 'string',
			'description'  => 'Content placed between the header slider and the page title.',
			'single'       => true,
			'show_in_rest' => true,
		] );

		register_meta( 'post', 'page-middle-header-content', [
			'type'         => 'string',
			'description'  => 'Content placed between the header slider and the page title.',
			'single'       => true,
			'show_in_rest' => true,
		] );

		register_meta( 'post', 'description', [
			'type'         => 'string',
			'description'  => 'Similar to an excerpt, this text will be shown next to the title in a single page/post view.',
			'single'       => true,
			'show_in_rest' => true,
		] );
	}

	public static function add_meta_boxes() {

	}
}