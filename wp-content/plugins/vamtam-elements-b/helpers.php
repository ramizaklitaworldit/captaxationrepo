<?php

// manual css cache regeneration
function vamtam_setup_adminbar() {
	if ( ! current_user_can( 'edit_theme_options' ) || ! class_exists( 'VamtamFramework' ) ) {
		return;
	}

	global $wp_admin_bar;

	$wp_admin_bar->add_menu( array(
		'parent' => false,
		'id'     => 'vamtam_theme_options',
		'title'  => esc_html__( 'VamTam', 'vamtam-elements-b' ),
		'href'   => '',
		'meta'   => false,
	) );

	$wp_admin_bar->add_menu(array(
		'parent' => 'vamtam_theme_options',
		'id'     => 'vamtam_clear_css_cache',
		'title'  => esc_html__( 'Clear Cache', 'vamtam-elements-b' ),
		'href'   => admin_url( 'admin.php?vamtam_action=clear_cache' ),
		'meta'   => false,
	) );
}
add_action( 'wp_before_admin_bar_render', 'vamtam_setup_adminbar', 11 );

/**
 * Map an accent name to its value
 *
 * @param  string      $color           accent name
 * @param  string|bool $deprecated
 * @return string                       hex color or the input string
 */
function vamtam_el_sanitize_accent( $color, $deprecated = false ) {
	if ( function_exists( 'vamtam_sanitize_accent' ) ) {
		return vamtam_sanitize_accent( $color );
	}

	if ( preg_match( '/accent(?:-color-)?(\d)/i', $color, $matches ) ) {
		$num = (int) $matches[1];

		$color = "var( --vamtam-accent-color-{$num} )";
	}

	return $color;
}

function vamtam_el_after_setup_theme() {
	if ( ! function_exists( 'vamtam_sanitize_bool' ) ) {
		/**
		 * Converts '1', '0', 'true' and 'false' to booleans, otherwise returns $value
		 * @param  mixed $value original value
		 * @return mixed        sanitized value
		 */
		function vamtam_sanitize_bool( $value ) {
			if ( $value === '1' || $value === 'true' ) {
				return true;
			}

			if ( $value === '0' || $value === 'false' ) {
				return false;
			}

			return $value;
		}
	}
}
add_action( 'after_setup_theme', 'vamtam_el_after_setup_theme' );