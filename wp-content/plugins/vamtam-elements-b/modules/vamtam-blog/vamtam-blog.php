<?php

/**
 * @class VamtamBlogModule
 */
class VamtamBlogModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct() {
		$path = trailingslashit( 'modules/' . basename( dirname( __FILE__ ) ) );

		parent::__construct(array(
			'name'            => __( 'Blog', 'vamtam-elements-b' ),
			'description'     => __( 'Display your WordPress posts.', 'vamtam-elements-b' ),
			'category'        => __( 'VamTam Modules', 'vamtam-elements-b' ),
			'partial_refresh' => true,
			'editor_export'   => false,
			'dir'             => VAMTAMEL_B_DIR . $path,
			'url'             => VAMTAMEL_B_URL . $path,
		));
	}

	/**
	 * Renders the schema structured data for the current
	 * post in the loop.
	 *
	 * @since 1.7.4
	 * @return void
	 */
	static public function schema_meta() {
		global $vamtam_theme;

		// General Schema Meta
		echo '<meta itemprop="datePublished" content="' . esc_attr( get_the_time( 'Y-m-d' ) ) . '" />';
		echo '<meta itemprop="dateModified" content="' . esc_attr( get_the_modified_date( 'Y-m-d' ) ) . '" />';

		echo '<meta itemprop="headline" content="' . esc_attr( get_the_title() ) . '">';

		// Publisher Schema Meta
		echo '<div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">';
		echo '<meta itemprop="name" content="' . esc_attr( get_bloginfo( 'name' ) ) . '">';
		echo '<meta itemprop="url" content="' . esc_attr( get_home_url() ) . '">';

		if ( isset( $vamtam_theme['header-logo-type'] ) && 'image' === $vamtam_theme['header-logo-type'] ) {
			echo '<div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">';
			echo '<meta itemprop="url" content="' . esc_url( wp_get_attachment_image_src( get_theme_mod( 'custom_logo' ), 'full' )[0] ) . '">';
			echo '</div>';
		}

		echo '</div>';

		// Author Schema Meta
		echo '<div itemscope itemprop="author" itemtype="http://schema.org/Person">';
		echo '<meta itemprop="url" content="' . esc_attr( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '" />';
		echo '<meta itemprop="name" content="' . esc_attr( get_the_author_meta( 'display_name', get_the_author_meta( 'ID' ) ) ) . '" />';
		echo '</div>';

		// Image Schema Meta
		if ( has_post_thumbnail() ) {

			$image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' );

			if ( is_array( $image ) ) {
				echo '<div itemscope itemprop="image" itemtype="http://schema.org/ImageObject">';
				echo '<meta itemprop="url" content="' . esc_url( $image[0] ) . '" />';
				echo '<meta itemprop="width" content="' . intval( $image[1] ) . '" />';
				echo '<meta itemprop="height" content="' . intval( $image[2] ) . '" />';
				echo '</div>';
			}
		}

		// Comment Schema Meta
		echo '<div itemprop="interactionStatistic" itemscope itemtype="http://schema.org/InteractionCounter">';
		echo '<meta itemprop="interactionType" content="http://schema.org/CommentAction" />';
		echo '<meta itemprop="userInteractionCount" content="' . esc_attr( wp_count_comments( get_the_ID() )->approved ) . '" />';
		echo '</div>';
	}

	/**
	 * Renders the schema itemtype for the current
	 * post in the loop.
	 *
	 * @since 1.7.4
	 * @return void
	 */
	static public function schema_itemtype() {
		global $post;

		if ( ! is_object( $post ) || ! isset( $post->post_type ) || 'post' !== $post->post_type ) {
			echo 'http://schema.org/CreativeWork';
		} else {
			echo 'http://schema.org/BlogPosting';
		}
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('VamtamBlogModule', array(
	'layout' => array(
		'title'    => __( 'Layout', 'vamtam-elements-b' ),
		'sections' => array(
			'general' => array(
				'title'  => '',
				'fields' => array(
					'layout' => array(
						'type'    => 'select',
						'label'   => __( 'Layout', 'vamtam-elements-b' ),
						'default' => 'small',

						'options' => array(
							'normal'   => esc_html__( 'Big Images', 'vamtam-elements-b' ),
							'small'    => esc_html__( 'Small Images - Normal', 'vamtam-elements-b' ),
							'scroll-x' => esc_html__( 'Small Images - Scrollable', 'vamtam-elements-b' ),
							'mosaic'   => esc_html__( 'Small Images - Masonry', 'vamtam-elements-b' ),
						),
						'toggle' => array(
							'normal' => array(
								'fields' => array( 'pagination' ),
							),
							'small' => array(
								'sections' => array( 'grid' ),
								'fields'   => array( 'pagination', 'show_title' ),
							),
							'scroll-x' => array(
								'sections' => array( 'grid' ),
								'fields'   => array( 'show_title' ),
							),
							'mosaic' => array(
								'sections' => array( 'grid' ),
								'fields'   => array( 'pagination', 'show_title' ),
							),
						),
					),
					'pagination' => array(
						'type'    => 'select',
						'label'   => __( 'Pagination', 'vamtam-elements-b' ),
						'default' => 'true',
						'options' => array(
							'true'  => esc_html__( 'On', 'vamtam-elements-b' ),
							'false' => esc_html__( 'Off', 'vamtam-elements-b' ),
						),
					),
					'posts_per_page' => array(
						'type'    => 'unit',
						'label'   => esc_html__( 'Posts Per Page', 'vamtam-elements-b' ),
						'default' => 10,
						'min'     => -1,
						'max'     => 100,
					),
				),
			),
			'grid' => array(
				'title'  => __( 'Grid', 'vamtam-elements-b' ),
				'fields' => array(
					'columns' => array(
						'type'    => 'unit',
						'label'   => esc_html__( 'Columns', 'vamtam-elements-b' ),
						'default' => 3,
						'min'     => 2,
						'max'     => 4,
					),
					'gap' => array(
						'type'    => 'select',
						'label'   => esc_html__( 'Gap Between Items', 'vamtam-elements-b' ),
						'default' => 'true',
						'options' => array(
							'true'  => esc_html__( 'On', 'vamtam-elements-b' ),
							'false' => esc_html__( 'Off', 'vamtam-elements-b' ),
						),
					),
				),
			),
			'content' => array(
				'title'  => __( 'Content', 'vamtam-elements-b' ),
				'fields' => array(
					'show_title' => array(
						'type'    => 'select',
						'label'   => __( 'Post Title', 'vamtam-elements-b' ),
						'default' => '1',
						'options' => array(
							'1' => __( 'Show', 'vamtam-elements-b' ),
							'0' => __( 'Hide', 'vamtam-elements-b' ),
						),
					),
					'show_content' => array(
						'type'    => 'select',
						'label'   => __( 'Post Content', 'vamtam-elements-b' ),
						'default' => '1',
						'options' => array(
							'1' => __( 'Show', 'vamtam-elements-b' ),
							'0' => __( 'Hide', 'vamtam-elements-b' ),
						),
					),
					'show_media' => array(
						'type'    => 'select',
						'label'   => __( 'Show Media (if available)', 'vamtam-elements-b' ),
						'default' => '1',
						'options' => array(
							'1' => __( 'Show', 'vamtam-elements-b' ),
							'0' => __( 'Hide', 'vamtam-elements-b' ),
						),
					),
				),
			),
		),
	),
	'content' => array(
		'title' => __( 'Query', 'vamtam-elements-b' ),
		'file'  => FL_BUILDER_DIR . 'includes/loop-settings.php',
	),
));
