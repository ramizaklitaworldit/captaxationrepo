<div class="fl-map">
	<?php $attribute = FLBuilderModel::is_builder_active() ? 'src' : 'src="about:blank" data-src' ?>
	<iframe <?= $attribute //xss ok ?>="https://maps.google.com/maps?q=<?php echo urlencode( $settings->address ); ?>&iwloc=near&output=embed" height="<?php echo (int)$settings->height; ?>" frameborder="0" style="border:0;width:100%"></iframe>
</div>
