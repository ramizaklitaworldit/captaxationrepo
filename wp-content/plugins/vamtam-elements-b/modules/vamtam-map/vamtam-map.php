<?php

/**
 * @class VamtamMapModule
 */
class VamtamMapModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct() {
		$path = trailingslashit( 'modules/' . basename( dirname( __FILE__ ) ) );

		parent::__construct(array(
			'name'            => __( 'Map', 'vamtam-elements-b' ),
			'description'     => __( 'Display a Google map.', 'vamtam-elements-b' ),
			'category'        => __( 'VamTam Modules', 'vamtam-elements-b' ),
			'partial_refresh' => true,
			'dir'             => VAMTAMEL_B_DIR . $path,
			'url'             => VAMTAMEL_B_URL . $path,
		));
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('VamtamMapModule', array(
	'general' => array(
		'title'    => __( 'General', 'vamtam-elements-b' ),
		'sections' => array(
			'general' => array(
				'title'  => '',
				'fields' => array(
					'address' => array(
						'type'        => 'text',
						'label'       => __( 'Address', 'vamtam-elements-b' ),
						'placeholder' => __( '1865 Winchester Blvd #202 Campbell, CA 95008', 'vamtam-elements-b' ),
						'preview'     => array(
							'type' => 'refresh',
						),
						'vamtam-wpml' => 'LINE',
					),
					'height' => array(
						'type'        => 'text',
						'label'       => __( 'Height', 'vamtam-elements-b' ),
						'default'     => '400',
						'size'        => '5',
						'description' => 'px',
					),
				),
			),
		),
	),
));
