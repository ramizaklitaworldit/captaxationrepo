<?php

/**
 * @class VamtamTestimonialsModule
 */
class VamtamTestimonialsModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct() {
		$path = trailingslashit( 'modules/' . basename( dirname( __FILE__ ) ) );

		parent::__construct( array(
			'name'            => __( 'Testimonials', 'vamtam-elements-b' ),
			'description'     => __( 'Display customer testimonials.', 'vamtam-elements-b' ),
			'category'        => __( 'VamTam Modules', 'vamtam-elements-b' ),
			'partial_refresh' => true,
			'editor_export'   => false,
			'dir'             => VAMTAMEL_B_DIR . $path,
			'url'             => VAMTAMEL_B_URL . $path,
		));
	}

	/**
	 * Renders the schema structured data for the current
	 * post in the loop.
	 *
	 * @since 1.7.4
	 * @return void
	 */
	static public function schema_meta() {
		global $vamtam_theme;

		// General Schema Meta
		echo '<meta itemscope itemprop="mainEntityOfPage" itemid="' . esc_attr( get_permalink() ) . '" />';
		echo '<meta itemprop="datePublished" content="' . esc_attr( get_the_time( 'Y-m-d' ) ) . '" />';
		echo '<meta itemprop="dateModified" content="' . esc_attr( get_the_modified_date( 'Y-m-d' ) ) . '" />';

		// Publisher Schema Meta
		echo '<div itemprop="publisher" itemscope itemtype="https://schema.org/Organization">';
		echo '<meta itemprop="name" content="' . esc_attr( get_bloginfo( 'name' ) ) . '">';
		echo '<meta itemprop="url" content="' . esc_attr( get_home_url() ) . '">';

		if ( isset( $vamtam_theme['header-logo-type'] ) && 'image' === $vamtam_theme['header-logo-type'] ) {
			echo '<div itemprop="logo" itemscope itemtype="https://schema.org/ImageObject">';
			echo '<meta itemprop="url" content="' . esc_url( $vamtam_theme['custom-header-logo'] ) . '">';
			echo '</div>';
		}

		echo '</div>';

		// Image Schema Meta
		if ( has_post_thumbnail() ) {

			$image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' );

			if ( is_array( $image ) ) {
				echo '<div itemscope itemprop="image" itemtype="http://schema.org/ImageObject">';
				echo '<meta itemprop="url" content="' . esc_url( $image[0] ) . '" />';
				echo '<meta itemprop="width" content="' . intval( $image[1] ) . '" />';
				echo '<meta itemprop="height" content="' . intval( $image[2] ) . '" />';
				echo '</div>';
			}
		}
	}

	/**
	 * Renders the schema itemtype for the current
	 * post in the loop.
	 *
	 * @since 1.7.4
	 * @return void
	 */
	static public function schema_itemtype() {
		echo 'http://schema.org/Review';
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('VamtamTestimonialsModule', array(
	'layout' => array(
		'title'    => __( 'Layout', 'vamtam-elements-b' ),
		'sections' => array(
			'general' => array(
				'title'  => '',
				'fields' => array(
					'layout_type' => array(
						'type'    => 'select',
						'label'   => __( 'Type', 'vamtam-elements-b' ),
						'default' => 'slider',
						'options' => array(
							'list'   => esc_html__( 'List', 'vamtam-elements-b' ),
							'slider' => esc_html__( 'Slider', 'vamtam-elements-b' ),
						),
						'toggle' => array(
							'list' => array(
								'fields' => array( 'pagination' ),
							),
							'slider' => array(
								'sections' => array( 'slider' ),
							),
						),
					),
					'alignment' => array(
						'type'    => 'select',
						'label'   => __( 'Alignment', 'vamtam-elements-b' ),
						'default' => 'left',
						'options' => array(
							'left'   => esc_html__( 'Left', 'vamtam-elements-b' ),
							'center' => esc_html__( 'Center', 'vamtam-elements-b' ),
							'right'  => esc_html__( 'Right', 'vamtam-elements-b' ),
						),
					),
					'pagination' => array(
						'type'    => 'select',
						'label'   => esc_html__( 'Pagination', 'vamtam-elements-b' ),
						'default' => '0',
						'options' => array(
							'1' => esc_html__( 'Enabled', 'vamtam-elements-b' ),
							'0' => esc_html__( 'Disabled', 'vamtam-elements-b' ),
						),
					),
				),
			),
			'content' => array(
				'title'  => __( 'Content', 'vamtam-elements-b' ),
				'fields' => array(
					'show_rating' => array(
						'type'    => 'select',
						'label'   => __( 'Show Rating', 'vamtam-elements-b' ),
						'default' => '1',
						'options' => array(
							'1' => __( 'Show', 'vamtam-elements-b' ),
							'0' => __( 'Hide', 'vamtam-elements-b' ),
						),
					),
				),
			),
			'slider' => array(
				'title'  => __( 'Slider', 'vamtam-elements-b' ),
				'fields' => array(
					'autorotate' => array(
						'type'    => 'select',
						'label'   => __( 'Rotate Automatically', 'vamtam-elements-b' ),
						'default' => '1',
						'options' => array(
							'1' => __( 'Yes', 'vamtam-elements-b' ),
							'0' => __( 'No', 'vamtam-elements-b' ),
						),
					),
				),
			),
		),
	),
	'query' => array(
		'title' => __( 'Query', 'vamtam-elements-b' ),
		'file'  => dirname( __FILE__ ) . '/loop-settings.php',
	),
));
