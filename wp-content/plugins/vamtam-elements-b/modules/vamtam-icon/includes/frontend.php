<?php
	$has_lightbox = vamtam_sanitize_bool( $settings->lightbox_embed );
	$button_attrs = '';

	if ( $has_lightbox ) {
		global $wp_embed, $content_width;

		$lightbox_content = do_shortcode( $wp_embed->run_shortcode( '[embed width="' . esc_attr( $content_width ) . '"]' . $settings->link . '[/embed]' ) );

		$button_attrs .= ' data-vamtam-lightbox="' . esc_attr( $lightbox_content ) . '"';

		$settings->link_target = '_blank';

		Vamtam_Elements_B::enqueue_lightbox_template();
	}

	$icon_content = '';

	if ( function_exists( 'vamtam_get_icon' ) && strpos( $settings->icon, 'vamtam-theme-' ) === 0 ) {
		$icon_content = 'data-icon="' . esc_attr( vamtam_get_icon( $settings->icon ) ) . '"';
		$settings->icon = 'icon icon-b theme';
	}

?>
<?php if ( ! isset( $settings->exclude_wrapper ) ) : ?>
<span class="fl-icon-wrap">
<?php endif; ?>
	<span class="fl-icon">
		<?php if ( ! empty( $settings->link ) ) : ?>
		<a href="<?php echo esc_url( $settings->link ) ?>" target="<?php echo esc_attr( $settings->link_target ) ?>" <?php echo $button_attrs // xss ok ?>>
			<?php if ( ! empty( $settings->screen_reader_text ) ): ?>
				<span class="screen-reader-text"><?= esc_html( $settings->screen_reader_text ) ?></span>
			<?php endif ?>
		<?php endif; ?>
			<i class="<?php echo esc_attr( $settings->icon ) ?> <?php if ( $settings->bg_color ) echo 'has-background' ?>" <?php echo $icon_content // xss ok ?>></i>
		<?php if ( ! empty( $settings->link ) ) : ?></a><?php endif; ?>
	</span>

	<?php if ( ! empty( $settings->text ) ) : ?>
	<span class="fl-icon-text">
		<?php if ( ! empty( $settings->link ) ) : ?>
		<a href="<?php echo esc_url( $settings->link ) ?>" target="<?php echo esc_attr( $settings->link_target ) ?>" <?php echo $button_attrs // xss ok ?>>
		<?php endif; ?>
			<?php echo strip_tags( $settings->text, '<span><bold><b><i><em><br>' ); // xss ok ?>
		<?php if ( ! empty( $settings->link ) ) : ?></a><?php endif; ?>
	</span>
	<?php endif; ?>
<?php if ( ! isset( $settings->exclude_wrapper ) ) : ?>
</span>
<?php endif; ?>
