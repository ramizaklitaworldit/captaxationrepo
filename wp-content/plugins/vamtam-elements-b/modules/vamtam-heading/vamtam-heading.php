<?php

/**
 * @class VamtamHeadingModule
 */
class VamtamHeadingModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct() {
		$path = trailingslashit( 'modules/' . basename( dirname( __FILE__ ) ) );

		parent::__construct(array(
			'name'            => esc_html__( 'Heading', 'vamtam-elements-b' ),
			'description'     => esc_html__( 'Display a title/page heading.', 'vamtam-elements-b' ),
			'category'        => esc_html__( 'VamTam Modules', 'vamtam-elements-b' ),
			'partial_refresh' => true,
			'dir'             => VAMTAMEL_B_DIR . $path,
			'url'             => VAMTAMEL_B_URL . $path,
		));
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('VamtamHeadingModule', array(
	'vamtam-heading' => array(
		'title'    => esc_html__( 'General', 'vamtam-elements-b' ),
		'sections' => array(
			'general' => array(
				'title'  => '',
				'fields' => array(
					'heading' => array(
						'type'    => 'text',
						'label'   => esc_html__( 'Heading', 'vamtam-elements-b' ),
						'default' => '',
						'preview' => array(
							'type'     => 'text',
							'selector' => '.vamtam-heading-text',
						),
						'vamtam-wpml' => 'LINE',
					),
				),
			),
			'link' => array(
				'title'  => esc_html__( 'Link', 'vamtam-elements-b' ),
				'fields' => array(
					'link' => array(
						'type'    => 'link',
						'label'   => esc_html__( 'Link', 'vamtam-elements-b' ),
						'preview' => array(
							'type' => 'none',
						),
					),
					'link_target' => array(
						'type'    => 'select',
						'label'   => esc_html__( 'Link Target', 'vamtam-elements-b' ),
						'default' => '_self',
						'options' => array(
							'_self'  => esc_html__( 'Same Window', 'vamtam-elements-b' ),
							'_blank' => esc_html__( 'New Window', 'vamtam-elements-b' ),
						),
						'preview' => array(
							'type' => 'none',
						),
					),
				),
			),
		),
	),
	'style' => array(
		'title'    => esc_html__( 'Style', 'vamtam-elements-b' ),
		'sections' => array(
			'colors' => array(
				'title'  => esc_html__( 'Colors', 'vamtam-elements-b' ),
				'fields' => array(
					'color' => array(
						'type'       => 'vamtam-color',
						'label'      => esc_html__( 'Text Color', 'vamtam-elements-b' ),
					),
				),
			),
			'structure' => array(
				'title'  => esc_html__( 'Structure', 'vamtam-elements-b' ),
				'fields' => array(
					'with_divider' => array(
						'type'    => 'select',
						'label'   => esc_html__( 'Divider', 'vamtam-elements-b' ),
						'default' => 'off',
						'options' => array(
							'on'  => esc_html__( 'On', 'vamtam-elements-b' ),
							'off' => esc_html__( 'Off', 'vamtam-elements-b' ),
						),
					),
					'alignment' => array(
						'type'    => 'select',
						'label'   => esc_html__( 'Alignment', 'vamtam-elements-b' ),
						'default' => 'left',
						'options' => array(
							'left'   => esc_html__( 'Left', 'vamtam-elements-b' ),
							'center' => esc_html__( 'Center', 'vamtam-elements-b' ),
							'right'  => esc_html__( 'Right', 'vamtam-elements-b' ),
						),
					),
					'tag' => array(
						'type'    => 'select',
						'label'   => esc_html__( 'HTML Tag', 'vamtam-elements-b' ),
						'default' => 'h3',
						'options' => array(
							'h1' => 'h1',
							'h2' => 'h2',
							'h3' => 'h3',
							'h4' => 'h4',
							'h5' => 'h5',
							'h6' => 'h6',
						),
					),
					'style_base' => array(
						'type'    => 'select',
						'label'   => esc_html__( 'Style Base', 'vamtam-elements-b' ),
						'default' => 'tag',
						'options' => array(
							'tag'     => esc_html__( 'From HTML tag', 'vamtam-elements-b' ),
							'style-1' => esc_html__( 'Additional Font Style 1', 'vamtam-elements-b' ),
							'style-2' => esc_html__( 'Additional Font Style 2', 'vamtam-elements-b' ),
						),
					),
					'font' => array(
						'type'    => 'font',
						'default' => array(
							'family' => 'Default',
							'weight' => 300,
						),
						'label'   => esc_html__( 'Font', 'vamtam-elements-b' ),
						'preview' => array(
							'type'     => 'font',
							'selector' => '.vamtam-heading-text',
						),
					),
					'font_size' => array(
						'type'    => 'select',
						'label'   => esc_html__( 'Font Size', 'vamtam-elements-b' ),
						'default' => 'default',
						'options' => array(
							'default' => esc_html__( 'Default', 'vamtam-elements-b' ),
							'custom'  => esc_html__( 'Custom', 'vamtam-elements-b' ),
						),
						'toggle' => array(
							'custom' => array(
								'fields' => array( 'custom_font_size' ),
							),
						),
					),
					'custom_font_size' => array(
						'type'        => 'text',
						'label'       => esc_html__( 'Custom Font Size', 'vamtam-elements-b' ),
						'default'     => '24',
						'maxlength'   => '3',
						'size'        => '4',
						'description' => 'px',
					),
					'line_height' => array(
						'type'    => 'select',
						'label'   => esc_html__( 'Line Height', 'vamtam-elements-b' ),
						'default' => 'default',
						'options' => array(
							'default' => esc_html__( 'Default', 'vamtam-elements-b' ),
							'custom'  => esc_html__( 'Custom', 'vamtam-elements-b' ),
						),
						'toggle' => array(
							'custom' => array(
								'fields' => array( 'custom_line_height' ),
							),
						),
					),
					'custom_line_height' => array(
						'type'        => 'text',
						'label'       => esc_html__( 'Custom Line Height', 'vamtam-elements-b' ),
						'default'     => '1.4',
						'maxlength'   => '4',
						'size'        => '4',
						'description' => 'em',
					),
					'letter_spacing' => array(
						'type'    => 'select',
						'label'   => esc_html__( 'Letter Spacing', 'vamtam-elements-b' ),
						'default' => 'default',
						'options' => array(
							'default' => esc_html__( 'Default', 'vamtam-elements-b' ),
							'custom'  => esc_html__( 'Custom', 'vamtam-elements-b' ),
						),
						'toggle' => array(
							'custom' => array(
								'fields' => array( 'custom_letter_spacing' ),
							),
						),
					),
					'custom_letter_spacing' => array(
						'type'        => 'text',
						'label'       => esc_html__( 'Custom Letter Spacing', 'vamtam-elements-b' ),
						'default'     => '0',
						'maxlength'   => '3',
						'size'        => '4',
						'description' => 'px',
					),
				),
			),
			'r_structure' => array(
				'title'  => esc_html__( 'Mobile Structure', 'vamtam-elements-b' ),
				'fields' => array(
					'r_alignment' => array(
						'type'    => 'select',
						'label'   => esc_html__( 'Alignment', 'vamtam-elements-b' ),
						'default' => 'default',
						'options' => array(
							'default' => esc_html__( 'Default', 'vamtam-elements-b' ),
							'custom'  => esc_html__( 'Custom', 'vamtam-elements-b' ),
						),
						'toggle' => array(
							'custom' => array(
								'fields' => array( 'r_custom_alignment' ),
							),
						),
						'preview' => array(
							'type' => 'none',
						),
					),
					'r_custom_alignment' => array(
						'type'    => 'select',
						'label'   => esc_html__( 'Custom Alignment', 'vamtam-elements-b' ),
						'default' => 'center',
						'options' => array(
							'left'   => esc_html__( 'Left', 'vamtam-elements-b' ),
							'center' => esc_html__( 'Center', 'vamtam-elements-b' ),
							'right'  => esc_html__( 'Right', 'vamtam-elements-b' ),
						),
						'preview' => array(
							'type' => 'none',
						),
					),
					'r_font_size' => array(
						'type'    => 'select',
						'label'   => esc_html__( 'Font Size', 'vamtam-elements-b' ),
						'default' => 'default',
						'options' => array(
							'default' => esc_html__( 'Default', 'vamtam-elements-b' ),
							'custom'  => esc_html__( 'Custom', 'vamtam-elements-b' ),
						),
						'toggle' => array(
							'custom' => array(
								'fields' => array( 'r_custom_font_size' ),
							),
						),
						'preview' => array(
							'type' => 'none',
						),
					),
					'r_custom_font_size' => array(
						'type'        => 'text',
						'label'       => esc_html__( 'Custom Font Size', 'vamtam-elements-b' ),
						'default'     => '24',
						'maxlength'   => '3',
						'size'        => '4',
						'description' => 'px',
						'preview'     => array(
							'type' => 'none',
						),
					),
					'r_line_height' => array(
						'type'    => 'select',
						'label'   => esc_html__( 'Line Height', 'vamtam-elements-b' ),
						'default' => 'default',
						'options' => array(
							'default' => esc_html__( 'Default', 'vamtam-elements-b' ),
							'custom'  => esc_html__( 'Custom', 'vamtam-elements-b' ),
						),
						'toggle' => array(
							'custom' => array(
								'fields' => array( 'r_custom_line_height' ),
							),
						),
					),
					'r_custom_line_height' => array(
						'type'      => 'text',
						'label'     => esc_html__( 'Custom Line Height', 'vamtam-elements-b' ),
						'default'   => '1.4',
						'maxlength' => '4',
						'size'      => '4',
					),
					'r_letter_spacing' => array(
						'type'    => 'select',
						'label'   => esc_html__( 'Letter Spacing', 'vamtam-elements-b' ),
						'default' => 'default',
						'options' => array(
							'default' => esc_html__( 'Default', 'vamtam-elements-b' ),
							'custom'  => esc_html__( 'Custom', 'vamtam-elements-b' ),
						),
						'toggle' => array(
							'custom' => array(
								'fields' => array( 'r_custom_letter_spacing' ),
							),
						),
					),
					'r_custom_letter_spacing' => array(
						'type'        => 'text',
						'label'       => esc_html__( 'Custom Letter Spacing', 'vamtam-elements-b' ),
						'default'     => '0',
						'maxlength'   => '3',
						'size'        => '4',
						'description' => 'px',
					),
				),
			),
		),
	),
));
