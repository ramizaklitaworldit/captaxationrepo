<?php

FLBuilder::render_module_css( 'vamtam-icon', $id, array(
	'align'          => '',
	'bg_color'       => $settings->bg_color,
	'bg_hover_color' => $settings->bg_hover_color,
	'color'          => $settings->color,
	'hover_color'    => $settings->hover_color,
	'icon'           => '',
	'link'           => '',
	'link_target'    => '',
	'size'           => $settings->size,
	'text'           => '',
) );

?>
<?php foreach ( $settings->icons as $i => $icon ) : ?>
	<?php if ( ( isset( $icon->color ) && ! empty( $icon->color ) ) || ( isset( $icon->bg_color ) && ! empty( $icon->bg_color ) ) ) : ?>
		.fl-node-<?php echo $id; ?> .fl-module-content .fl-icon:nth-child(<?php echo $i + 1; ?>) i {
			<?php if ( ! empty( $icon->color ) ) : ?>
				color: <?php echo esc_html( vamtam_el_sanitize_accent( $icon->color ) ) ?>;
			<?php endif; ?>
			<?php if ( ! empty( $icon->bg_color ) ) : ?>
				background: <?php echo esc_html( vamtam_el_sanitize_accent( $icon->bg_color ) ) ?>;
			<?php endif; ?>
		}
	<?php endif; ?>
	<?php if ( ( isset( $icon->hover_color ) && ! empty( $icon->hover_color ) ) || ( isset( $icon->bg_hover_color ) && ! empty( $icon->bg_hover_color ) ) ) : ?>
		.fl-node-<?php echo $id; ?> .fl-module-content .fl-icon:nth-child(<?php echo $i + 1; ?>) i:hover,
		.fl-node-<?php echo $id; ?> .fl-module-content .fl-icon:nth-child(<?php echo $i + 1; ?>) a:hover i {
			<?php if ( ! empty( $icon->hover_color ) ) : ?>
				color: <?php echo esc_html( vamtam_el_sanitize_accent( $icon->hover_color ) ) ?>;
			<?php endif; ?>
			<?php if ( ! empty( $icon->bg_hover_color ) ) : ?>
				background: <?php echo esc_html( vamtam_el_sanitize_accent( $icon->bg_hover_color ) ) ?>;
			<?php endif; ?>
		}
	<?php endif; ?>
<?php endforeach; ?>

.fl-node-<?php echo $id; ?> .fl-icon-group .fl-icon {
	display: inline-block;
	margin-bottom: 10px;
	margin-top: 10px;
	<?php if ( $settings->align === 'left' || $settings->align === 'center' ): ?>
		margin-right: <?php echo (int)$settings->spacing; ?>px;
	<?php endif ?>
	<?php if ( $settings->align === 'right' || $settings->align === 'center' ): ?>
		margin-left: <?php echo (int)$settings->spacing; ?>px;
	<?php endif ?>
}
