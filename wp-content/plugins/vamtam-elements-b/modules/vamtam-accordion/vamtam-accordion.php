<?php

/**
 * @class VamtamAccordionModule
 */
class VamtamAccordionModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct() {
		$path = trailingslashit( 'modules/' . basename( dirname( __FILE__ ) ) );

		parent::__construct(array(
			'name'            => __( 'Accordion', 'vamtam-elements-b' ),
			'description'     => __( 'Display a collapsible accordion of items.', 'vamtam-elements-b' ),
			'category'        => __( 'VamTam Modules', 'vamtam-elements-b' ),
			'partial_refresh' => true,
			'dir'             => VAMTAMEL_B_DIR . $path,
			'url'             => VAMTAMEL_B_URL . $path,
		));
	}

	public function enqueue_scripts() {
		$this->add_js( 'vamtam-accordion' );
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('VamtamAccordionModule', array(
	'items' => array(
		'title'    => __( 'Items', 'vamtam-elements-b' ),
		'sections' => array(
			'general' => array(
				'title'  => '',
				'fields' => array(
					'items' => array(
						'type'         => 'form',
						'label'        => __( 'Item', 'vamtam-elements-b' ),
						'form'         => 'accordion_items_form', // ID from registered form below
						'preview_text' => 'label', // Name of a field to use for the preview text
						'multiple'     => true,
					),
				),
			),
		),
	),
	'style' => array(
		'title'    => __( 'Style', 'vamtam-elements-b' ),
		'sections' => array(
			'general' => array(
				'title'  => '',
				'fields' => array(
					'border_color' => array(
						'type'    => 'vamtam-color',
						'label'   => __( 'Border Color', 'vamtam-elements-b' ),
						'default' => '',
					),
					'label_size' => array(
						'type'    => 'select',
						'label'   => __( 'Label Size', 'vamtam-elements-b' ),
						'default' => 'small',
						'options' => array(
							'small'  => _x( 'Small', 'Label size.', 'vamtam-elements-b' ),
							'medium' => _x( 'Medium', 'Label size.', 'vamtam-elements-b' ),
							'large'  => _x( 'Large', 'Label size.', 'vamtam-elements-b' ),
						),
						'preview' => array(
							'type' => 'none',
						),
					),
					'item_spacing' => array(
						'type'        => 'text',
						'label'       => __( 'Item Spacing', 'vamtam-elements-b' ),
						'default'     => '10',
						'maxlength'   => '2',
						'size'        => '3',
						'description' => 'px',
						'preview'     => array(
							'type' => 'none',
						),
					),
					'collapse' => array(
						'type'    => 'select',
						'label'   => __( 'Collapse Inactive', 'vamtam-elements-b' ),
						'default' => '1',
						'options' => array(
							'1' => __( 'Yes', 'vamtam-elements-b' ),
							'0' => __( 'No', 'vamtam-elements-b' ),
						),
						'help'    => __( 'Choosing yes will keep only one item open at a time. Choosing no will allow multiple items to be open at the same time.', 'vamtam-elements-b' ),
						'preview' => array(
							'type' => 'none',
						),
					),
					'open_first' => array(
						'type'    => 'select',
						'label'   => __( 'Expand First Item', 'vamtam-elements-b' ),
						'default' => '0',
						'options' => array(
							'0' => __( 'No', 'vamtam-elements-b' ),
							'1' => __( 'Yes', 'vamtam-elements-b' ),
						),
						'help' => __( 'Choosing yes will expand the first item by default.', 'vamtam-elements-b' ),
					),
				),
			),
		),
	),
));

/**
 * Register a settings form to use in the "form" field type above.
 */
FLBuilder::register_settings_form('accordion_items_form', array(
	'title' => __( 'Add Item', 'vamtam-elements-b' ),
	'tabs'  => array(
		'general' => array(
			'title'    => __( 'General', 'vamtam-elements-b' ),
			'sections' => array(
				'general' => array(
					'title'  => '',
					'fields' => array(
						'label' => array(
							'type'        => 'text',
							'label'       => __( 'Label', 'vamtam-elements-b' ),
							'vamtam-wpml' => 'line',
						),
					),
				),
				'content' => array(
					'title'  => __( 'Content', 'vamtam-elements-b' ),
					'fields' => array(
						'content' => array(
							'type'  => 'editor',
							'label' => '',
						),
					),
				),
			),
		),
	),
));
