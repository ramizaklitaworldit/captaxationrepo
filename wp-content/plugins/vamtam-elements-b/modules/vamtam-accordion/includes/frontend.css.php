.fl-node-<?php echo $id; ?> .fl-accordion-item {

	border: 1px solid;

	<?php if ( $settings->item_spacing == 0 ) : ?>

	border-bottom-width: 0;

	<?php else : ?>

	margin-bottom: <?php echo $settings->item_spacing; ?>px;

	<?php endif; ?>
}

<?php if ( $settings->item_spacing == 0 ) : ?>

.fl-node-<?php echo $id; ?> .fl-accordion-item:last-child {
	border-bottom-width: 1px;
}

<?php endif; ?>
