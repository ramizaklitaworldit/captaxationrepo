(function($){

	FLBuilder.registerModuleHelper('box-with-link', {

		rules: {
			title: {
				required: true
			},
			text: {
				required: true
			}
		},

		init: function() {}
	});

})(jQuery);
