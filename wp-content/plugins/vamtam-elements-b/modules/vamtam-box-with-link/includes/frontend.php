<a class="fl-bwl-wrap" href="<?php echo esc_url( $settings->link ); ?>" target="<?php echo esc_attr( $settings->link_target ); ?>">
    <?php if ( ! empty( $settings->icon ) ) : ?>
        <span class="fl-bwl-icon">
        <?php FLBuilder::render_module_html( 'vamtam-icon', array(
                'exclude_wrapper' => true,
                'color'           => $settings->icon_color,
                'icon'            => $settings->icon,
                'size'            => $settings->icon_size,
            ) );
        ?>
        </span>
    <?php endif ?>
    <span class="fl-bwl-content">
        <span class="fl-bwl-title font-h1"><?php echo wp_kses_post( strip_tags( $settings->title, '<a>' ) ) ?></span>
        <span class="fl-bwl-text"><?php echo wp_kses_post( strip_tags( $settings->text, '<p><br><a>' ) ) ?></span>
    </span>
    <span class="fl-bwl-hover-icon">
        <?php FLBuilder::render_module_html( 'vamtam-icon', array(
                'exclude_wrapper' => true,
                'icon'            => ! empty( $settings->hover_icon ) ? $settings->hover_icon : 'vamtam-theme-arrow-right-sample',
            ) );
        ?>
    </span>
</a>