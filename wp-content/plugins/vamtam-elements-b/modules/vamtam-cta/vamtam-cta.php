<?php

/**
 * @class VamtamCtaModule
 */
class VamtamCtaModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct() {
		$path = trailingslashit( 'modules/' . basename( dirname( __FILE__ ) ) );

		parent::__construct(array(
			'name'            => __( 'Call to Action', 'vamtam-elements-b' ),
			'description'     => __( 'Display a heading, subheading and a button.', 'vamtam-elements-b' ),
			'category'        => __( 'VamTam Modules', 'vamtam-elements-b' ),
			'partial_refresh' => true,
			'dir'             => VAMTAMEL_B_DIR . $path,
			'url'             => VAMTAMEL_B_URL . $path,
		));
	}

	/**
	 * @method get_classname
	 */
	public function get_classname() {

		$classname = 'fl-cta-wrap fl-cta-' . $this->settings->layout;

		if ( $this->settings->layout == 'stacked' ) {
			$classname .= ' fl-cta-' . $this->settings->alignment;
		}

		return $classname;
	}

	public static function get_button_settings( $settings ) {
		return array(
			'align'            => '',
			'color'            => $settings->btn_color,
			'hover_color'      => $settings->btn_hover_color,
			'font_size'        => $settings->btn_font_size,
			'icon'             => $settings->btn_icon,
			'icon_position'    => $settings->btn_icon_position,
			'link'             => $settings->btn_link,
			'link_target'      => $settings->btn_link_target,
			'padding'          => $settings->btn_padding,
			'layout_type'      => $settings->btn_layout_type,
			'text'             => $settings->btn_text,
			'width'            => $settings->layout == 'stacked' ? 'auto' : 'full',
		);
	}

	/**
	 * @method render_button
	 */
	public function render_button() {
		FLBuilder::render_module_html( 'vamtam-button', self::get_button_settings( $this->settings ) );
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('VamtamCtaModule', array(
	'general' => array(
		'title'    => __( 'General', 'vamtam-elements-b' ),
		'sections' => array(
			'content' => array(
				'title'  => '',
				'fields' => array(
					'title' => array(
						'type'    => 'text',
						'label'   => __( 'Heading', 'vamtam-elements-b' ),
						'default' => __( 'Ready to find out more?', 'vamtam-elements-b' ),
						'preview' => array(
							'type'     => 'text',
							'selector' => '.fl-cta-title',
						),
						'vamtam-wpml' => 'LINE',
					),
					'text' => array(
						'type'          => 'editor',
						'label'         => __( 'Text', 'vamtam-elements-b' ),
						'media_buttons' => false,
						'default'       => __( 'Drop us a line today for a free quote!', 'vamtam-elements-b' ),
						'preview'       => array(
							'type'     => 'text',
							'selector' => '.fl-cta-text-content',
						),
					),
					'image' => array(
						'type'  => 'photo',
						'label' => __( 'Image', 'fl-builder' ),
					),
				),
			),
		),
	),
	'style' => array(
		'title'    => __( 'Style', 'vamtam-elements-b' ),
		'sections' => array(
			'structure' => array(
				'title'  => __( 'Structure', 'vamtam-elements-b' ),
				'fields' => array(
					'layout' => array(
						'type'    => 'select',
						'label'   => __( 'Layout', 'vamtam-elements-b' ),
						'default' => 'inline',
						'options' => array(
							'inline'  => __( 'Inline', 'vamtam-elements-b' ),
							'stacked' => __( 'Stacked', 'vamtam-elements-b' ),
						),
						'toggle' => array(
							'stacked' => array(
								'fields' => array( 'alignment' ),
							),
						),
					),
					'alignment' => array(
						'type'    => 'select',
						'label'   => __( 'Alignment', 'vamtam-elements-b' ),
						'default' => 'center',
						'options' => array(
							'left'   => __( 'Left', 'vamtam-elements-b' ),
							'center' => __( 'Center', 'vamtam-elements-b' ),
							'right'  => __( 'Right', 'vamtam-elements-b' ),
						),
					),
					'spacing' => array(
						'type'        => 'text',
						'label'       => __( 'Spacing', 'vamtam-elements-b' ),
						'default'     => '0',
						'maxlength'   => '3',
						'size'        => '4',
						'description' => 'px',
						'preview'     => array(
							'type'     => 'css',
							'selector' => '.fl-module-content',
							'property' => 'padding',
							'unit'     => 'px',
						),
					),
				),
			),
			'title_structure' => array(
				'title'  => __( 'Heading Structure', 'vamtam-elements-b' ),
				'fields' => array(
					'title_tag' => array(
						'type'    => 'select',
						'label'   => __( 'Heading Tag', 'vamtam-elements-b' ),
						'default' => 'h3',
						'options' => array(
							'h1' => 'h1',
							'h2' => 'h2',
							'h3' => 'h3',
							'h4' => 'h4',
							'h5' => 'h5',
							'h6' => 'h6',
						),
					),
					'title_size' => array(
						'type'    => 'select',
						'label'   => __( 'Heading Size', 'vamtam-elements-b' ),
						'default' => 'default',
						'options' => array(
							'default' => __( 'Default', 'vamtam-elements-b' ),
							'custom'  => __( 'Custom', 'vamtam-elements-b' ),
						),
						'toggle' => array(
							'custom' => array(
								'fields' => array( 'title_custom_size' ),
							),
						),
					),
					'title_custom_size' => array(
						'type'        => 'text',
						'label'       => __( 'Heading Custom Size', 'vamtam-elements-b' ),
						'default'     => '24',
						'maxlength'   => '3',
						'size'        => '4',
						'description' => 'px',
					),
				),
			),
			'colors' => array(
				'title'  => __( 'Colors', 'vamtam-elements-b' ),
				'fields' => array(
					'text_color' => array(
						'type'       => 'vamtam-color',
						'label'      => __( 'Text Color', 'vamtam-elements-b' ),
						'default'    => '',
					),
					'bg_color' => array(
						'type'       => 'vamtam-color',
						'label'      => __( 'Background Color', 'vamtam-elements-b' ),
						'default'    => '',
					),
				),
			),
		),
	),
	'button' => array(
		'title'    => __( 'Button', 'vamtam-elements-b' ),
		'sections' => array(
			'btn_text' => array(
				'title'  => '',
				'fields' => array(
					'btn_text' => array(
						'type'    => 'text',
						'label'   => __( 'Text', 'vamtam-elements-b' ),
						'default' => __( 'Click Here', 'vamtam-elements-b' ),
						'preview' => array(
							'type'     => 'text',
							'selector' => '.vamtam-button-text',
						),
					),
					'btn_icon' => array(
						'type'        => 'icon',
						'label'       => __( 'Icon', 'vamtam-elements-b' ),
						'show_remove' => true,
					),
					'btn_icon_position' => array(
						'type'    => 'select',
						'label'   => __( 'Icon Position', 'vamtam-elements-b' ),
						'default' => 'before',
						'options' => array(
							'before' => __( 'Before Text', 'vamtam-elements-b' ),
							'after'  => __( 'After Text', 'vamtam-elements-b' ),
						),
					),
				),
			),
			'btn_link' => array(
				'title'  => __( 'Button Link', 'vamtam-elements-b' ),
				'fields' => array(
					'btn_link' => array(
						'type'    => 'link',
						'label'   => __( 'Link', 'vamtam-elements-b' ),
						'preview' => array(
							'type' => 'none',
						),
					),
					'btn_link_target' => array(
						'type'    => 'select',
						'label'   => __( 'Link Target', 'vamtam-elements-b' ),
						'default' => '_self',
						'options' => array(
							'_self'  => __( 'Same Window', 'vamtam-elements-b' ),
							'_blank' => __( 'New Window', 'vamtam-elements-b' ),
						),
						'preview' => array(
							'type' => 'none',
						),
					),
				),
			),
			'btn_colors' => array(
				'title'  => __( 'Button Colors', 'vamtam-elements-b' ),
				'fields' => array(
					'btn_color' => array(
						'type'    => 'select',
						'label'   => __( 'Normal Color', 'vamtam-elements-b' ),
						'default' => 'accent1',
						'options' => array(
							'accent1' => esc_html__( 'Accent 1', 'vamtam-elements-b' ),
							'accent2' => esc_html__( 'Accent 2', 'vamtam-elements-b' ),
							'accent3' => esc_html__( 'Accent 3', 'vamtam-elements-b' ),
							'accent4' => esc_html__( 'Accent 4', 'vamtam-elements-b' ),
							'accent5' => esc_html__( 'Accent 5', 'vamtam-elements-b' ),
							'accent6' => esc_html__( 'Accent 6', 'vamtam-elements-b' ),
							'accent7' => esc_html__( 'Accent 7', 'vamtam-elements-b' ),
							'accent8' => esc_html__( 'Accent 8', 'vamtam-elements-b' ),
						),
					),
					'btn_hover_color' => array(
						'type'    => 'select',
						'label'   => __( 'Hover Color', 'vamtam-elements-b' ),
						'default' => 'accent2',
						'options' => array(
							'accent1' => esc_html__( 'Accent 1', 'vamtam-elements-b' ),
							'accent2' => esc_html__( 'Accent 2', 'vamtam-elements-b' ),
							'accent3' => esc_html__( 'Accent 3', 'vamtam-elements-b' ),
							'accent4' => esc_html__( 'Accent 4', 'vamtam-elements-b' ),
							'accent5' => esc_html__( 'Accent 5', 'vamtam-elements-b' ),
							'accent6' => esc_html__( 'Accent 6', 'vamtam-elements-b' ),
							'accent7' => esc_html__( 'Accent 7', 'vamtam-elements-b' ),
							'accent8' => esc_html__( 'Accent 8', 'vamtam-elements-b' ),
						),
					),
				),
			),
			'btn_style' => array(
				'title'  => __( 'Button Style', 'vamtam-elements-b' ),
				'fields' => array(
					'btn_layout_type' => array(
						'type'    => 'select',
						'label'   => __( 'Button Type', 'vamtam-elements-b' ),
						'default' => 'solid',
						'options' => array(
							'solid'     => esc_html__( 'Solid', 'vamtam-elements-b' ),
							'border'    => esc_html__( 'Border', 'vamtam-elements-b' ),
							'underline' => esc_html__( 'Underline', 'vamtam-elements-b' ),
						),
					),
				),
			),
			'btn_structure' => array(
				'title'  => __( 'Button Structure', 'vamtam-elements-b' ),
				'fields' => array(
					'btn_width' => array(
						'type'    => 'select',
						'label'   => __( 'Button Width', 'vamtam-elements-b' ),
						'default' => 'auto',
						'options' => array(
							'auto' => _x( 'Auto', 'Width.', 'vamtam-elements-b' ),
							'full' => __( 'Full Width', 'vamtam-elements-b' ),
						),
					),
					'btn_font_size' => array(
						'type'        => 'text',
						'label'       => __( 'Font Size', 'vamtam-elements-b' ),
						'default'     => '14',
						'maxlength'   => '3',
						'size'        => '4',
						'description' => 'px',
					),
					'btn_padding' => array(
						'type'        => 'text',
						'label'       => __( 'Padding', 'vamtam-elements-b' ),
						'default'     => '10',
						'maxlength'   => '3',
						'size'        => '4',
						'description' => 'px',
					),
				),
			),
		),
	),
));
