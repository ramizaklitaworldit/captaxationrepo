<?php if ( $settings->title_size == 'custom' ) : ?>
.fl-builder-content .fl-node-<?php echo $id; ?> .fl-cta-title {
	font-size: <?php echo $settings->title_custom_size; ?>px;
}
<?php endif; ?>
<?php if ( is_numeric( $settings->spacing ) ) : ?>
.fl-node-<?php echo $id; ?> .fl-module-content {
	padding: <?php echo $settings->spacing; ?>px;
}
<?php endif; ?>
