(function($){

	FLBuilder.registerModuleHelper('cta', {

		rules: {
			title: {
				required: true
			},
			btn_text: {
				required: true
			}
		},

		init: function() {}
	});

})(jQuery);
