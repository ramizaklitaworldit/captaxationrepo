<?php

/**
 * @class VamtamCountdownModule
 */
class VamtamCountdownModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct() {
		$path = trailingslashit( 'modules/' . basename( dirname( __FILE__ ) ) );

		parent::__construct(array(
			'name'            => __( 'Countdown', 'vamtam-elements-b' ),
			'description'     => __( 'Render a Countdown module.', 'vamtam-elements-b' ),
			'category'        => __( 'VamTam Modules', 'vamtam-elements-b' ),
			'partial_refresh' => true,
			'dir'             => VAMTAMEL_B_DIR . $path,
			'url'             => VAMTAMEL_B_URL . $path,
		));
	}

	/**
	 * Builds an string with the respective ISO formatted time.
	 *
	 * @since 1.6.4
	 * @return string The current timestamp in an ISO format.
	 */
	public function get_time() {

		$year  = isset( $this->settings->year ) ? str_pad( $this->settings->year, 4, '0', STR_PAD_LEFT ) : '00';
		$month = isset( $this->settings->month ) ? str_pad( $this->settings->month, 2, '0', STR_PAD_LEFT ) : '00';
		$day   = isset( $this->settings->day ) ? str_pad( $this->settings->day, 2, '0', STR_PAD_LEFT ) : '00';

		$date = $year . '-' . $month . '-' . $day;

		$hours      = isset( $this->settings->time['hours'] ) ? str_pad( $this->settings->time['hours'], 2, '0', STR_PAD_LEFT ) : '00';
		$minutes    = isset( $this->settings->time['minutes'] ) ? str_pad( $this->settings->time['minutes'], 2, '0', STR_PAD_LEFT ) : '00';
		$day_period = isset( $this->settings->time['day_period'] ) ? $this->settings->time['day_period'] : 'AM';
		$zone       = isset( $this->settings->time_zone ) ? $this->settings->time_zone : date( 'e', current_time( 'timestamp', 1 ) );
		$time       = date( 'H:i:s', strtotime( $hours . ':' . $minutes . ':00 ' . strtoupper( $day_period ) ) );

		$timestamp = $date . ' ' . $time;

		$timezone = new DateTimeZone( $this->settings->time_zone );
		$date     = new DateTime( $timestamp, $timezone );

		return $date->format( 'c' );

	}

	/**
	 * Renders a svg circle for the current number.
	 *
	 * @since 1.6.4
	 * @return void
	 */
	public function render_circle() {

		$width  = ! empty( $this->settings->circle_width ) ? esc_attr( $this->settings->circle_width ) : 100;
		$pos    = ( $width / 2 );
		$radius = $pos - 10;
		$dash   = number_format( ( ( M_PI * 2 ) * $radius ), 2, '.', '' );

		$bg_stroke  = isset( $this->settings->circle_bg_color ) ? vamtam_el_sanitize_accent( $this->settings->circle_bg_color ) : '';
		$num_stroke = isset( $this->settings->circle_color ) ? vamtam_el_sanitize_accent( $this->settings->circle_color ) : '';

		$html  = '<div class="svg-container">';
		$html .= '<svg class="svg" viewBox="0 0 ' . $width . ' ' . $width . '" version="1.1" preserveAspectRatio="xMinYMin meet">
			<circle class="fl-number-bg" r="' . $radius . '" cx="' . $pos . '" cy="' . $pos . '" fill="transparent" stroke-dasharray="' . $dash . '" stroke-dashoffset="0" stroke="' . esc_attr( $bg_stroke ) . '"></circle>
			<circle class="fl-number" r="' . $radius . '" cx="' . $pos . '" cy="' . $pos . '" fill="transparent" stroke-dasharray="' . $dash . '" stroke-dashoffset="' . $dash . '" transform="rotate(-90 ' . $pos . ' ' . $pos . ')" stroke="' . esc_attr( $num_stroke ) . '"></circle>
		</svg>';
		$html .= '</div>';

		echo $html; // xss ok
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('VamtamCountdownModule', array(
	'general' => array(
		'title'    => __( 'General', 'vamtam-elements-b' ),
		'sections' => array(
			'date' => array(
				'title'  => __( 'Date', 'vamtam-elements-b' ),
				'fields' => array(
					'day' => array(
						'type'       => 'text',
						'label'      => __( 'Day', 'vamtam-elements-b' ),
						'default'    => date( 'j' ),
						'maxlength'  => '2',
						'size'       => '5',
						'preview' => array(
							'type' => 'none',
						),
					),
					'month' => array(
						'type'       => 'text',
						'label'      => __( 'Month', 'vamtam-elements-b' ),
						'default'    => date( 'n' ),
						'maxlength'  => '2',
						'size'       => '5',
						'preview' => array(
							'type' => 'none',
						),
					),
					'year' => array(
						'type'       => 'text',
						'label'      => __( 'Year', 'vamtam-elements-b' ),
						'default'    => date( 'Y' ),
						'maxlength'  => '4',
						'size'       => '5',
						'preview' => array(
							'type' => 'none',
						),
					),
				),
			),
			'time' => array(
				'title'  => __( 'Time', 'vamtam-elements-b' ),
				'fields' => array(
					'time' => array(
						'type'    => 'time',
						'label'   => __( 'Time', 'vamtam-elements-b' ),
						'default' => array(
							'hours'      => '01',
							'minutes'    => '00',
							'day_period' => 'am',
						),
					),
					'time_zone' => array(
						'type'    => 'timezone',
						'label'   => __( 'Time Zone', 'vamtam-elements-b' ),
						'default' => 'UTC',
					),
				),
			),

		),
	),
	'style' => array( // Tab
		'title'    => __( 'Style', 'vamtam-elements-b' ), // Tab title
		'sections' => array( // Tab Sections
			'general' => array(
				'title'  => '',
				'fields' => array(
					'layout' => array(
						'type'    => 'select',
						'label'   => __( 'Layout', 'vamtam-elements-b' ),
						'default' => 'default',
						'options' => array(
							'default' => __( 'Numbers', 'vamtam-elements-b' ),
							'circle'  => __( 'Numbers + Circles', 'vamtam-elements-b' ),
						),
						'toggle' => array(
							'circle' => array(
								'sections' => array( 'circle_bar_style' ),
								'fields'   => array( 'after_number_text' ),
							),
							'default' => array(
								'sections' => array( 'numbers_style', 'separator_style' ),
								'fields'   => array( 'horizontal_padding', 'vertical_padding' ),
							),
						),
					),
				),
			),
			'text_style' => array(
				'title'  => __( 'Numbers and Text', 'vamtam-elements-b' ),
				'fields' => array(
					'number_color' => array(
						'type'  => 'vamtam-color',
						'label' => __( 'Number Color', 'vamtam-elements-b' ),
					),
					'label_color' => array(
						'type'  => 'vamtam-color',
						'label' => __( 'Text Color', 'vamtam-elements-b' ),
					),
					'number_size' => array(
						'type'        => 'text',
						'label'       => __( 'Number Size', 'vamtam-elements-b' ),
						'default'     => '24',
						'maxlength'   => '3',
						'size'        => '4',
						'description' => 'px',
						'preview'  => array(
							'type'     => 'css',
							'selector' => '.fl-countdown .fl-countdown-unit-number',
							'property' => 'font-size',
							'unit'     => 'px',
						),
					),
					'label_size' => array(
						'type'        => 'text',
						'label'       => __( 'Text Size', 'vamtam-elements-b' ),
						'default'     => '13',
						'maxlength'   => '3',
						'size'        => '4',
						'description' => 'px',
						'preview'  => array(
							'type'     => 'css',
							'selector' => '.fl-countdown .fl-countdown-unit-label',
							'property' => 'font-size',
							'unit'     => 'px',
						),
					),
					'horizontal_padding' => array(
						'type'        => 'text',
						'label'       => __( 'Horizontal Padding', 'vamtam-elements-b' ),
						'default'     => '10',
						'maxlength'   => '3',
						'size'        => '4',
						'description' => 'px',
						'preview'  => array(
							'type'  => 'css',
							'rules' => array(
								array(
									'selector' => '.fl-countdown .fl-countdown-unit',
									'property' => 'padding-left',
									'unit'     => 'px',
								),
								array(
									'selector' => '.fl-countdown .fl-countdown-unit',
									'property' => 'padding-right',
									'unit'     => 'px',
								),
							),
						),
					),
					'vertical_padding' => array(
						'type'        => 'text',
						'label'       => __( 'Vertical Padding', 'vamtam-elements-b' ),
						'default'     => '10',
						'maxlength'   => '3',
						'size'        => '4',
						'description' => 'px',
						'preview'  => array(
							'type'  => 'css',
							'rules' => array(
								array(
									'selector' => '.fl-countdown .fl-countdown-unit',
									'property' => 'padding-top',
									'unit'     => 'px',
								),
								array(
									'selector' => '.fl-countdown .fl-countdown-unit',
									'property' => 'padding-bottom',
									'unit'     => 'px',
								),
							),
						),
					),
					'number_spacing' => array(
						'type'        => 'text',
						'label'       => __( 'Number Spacing', 'vamtam-elements-b' ),
						'default'     => '10',
						'maxlength'   => '3',
						'size'        => '4',
						'description' => 'px',
						'preview'  => array(
							'type'  => 'css',
							'rules' => array(
								array(
									'selector' => '.fl-countdown .fl-countdown-number',
									'property' => 'margin-left',
									'unit'     => 'px',
								),
								array(
									'selector' => '.fl-countdown .fl-countdown-number',
									'property' => 'margin-right',
									'unit'     => 'px',
								),
							),
						),
					),
				),
			),
			'numbers_style' => array(
				'title'  => __( 'Backgrounds', 'vamtam-elements-b' ),
				'fields' => array(
					'number_bg_color' => array(
						'type'       => 'vamtam-color',
						'label'      => __( 'Number Background Color', 'vamtam-elements-b' ),
					),
					'border_radius' => array(
						'type'        => 'text',
						'label'       => __( 'Number Border Radius', 'vamtam-elements-b' ),
						'default'     => '0',
						'maxlength'   => '3',
						'size'        => '4',
						'description' => 'px',
						'preview'  => array(
							'type'     => 'css',
							'selector' => '.fl-countdown .fl-countdown-unit',
							'property' => 'border-radius',
							'unit'     => 'px',
						),
					),
				),
			),
			'separator_style' => array(
				'title'  => __( 'Separator', 'vamtam-elements-b' ),
				'fields' => array(
					'show_separator' => array(
						'type'    => 'select',
						'label'   => __( 'Show Time Separators', 'vamtam-elements-b' ),
						'default' => 'no',
						'options' => array(
							'no'  => __( 'No', 'vamtam-elements-b' ),
							'yes' => __( 'Yes', 'vamtam-elements-b' ),
						),
						'toggle' => array(
							'yes' => array(
								'fields' => array( 'separator_type', 'separator_color' ),
							),
						),
					),
					'separator_type' => array(
						'type'    => 'select',
						'label'   => __( 'Separator Type', 'vamtam-elements-b' ),
						'default' => 'LINE',
						'options' => array(
							'colon' => __( 'Colon', 'vamtam-elements-b' ),
							'line'  => __( 'Line', 'vamtam-elements-b' ),
						),
						'toggle' => array(
							'colon' => array(
								'fields' => array( 'separator_size' ),
							),
						),
					),
					'separator_color' => array(
						'type'       => 'vamtam-color',
						'label'      => __( 'Separator Color', 'vamtam-elements-b' ),
					),
					'separator_size' => array(
						'type'        => 'text',
						'label'       => __( 'Separator Size', 'vamtam-elements-b' ),
						'default'     => '15',
						'maxlength'   => '3',
						'size'        => '4',
						'description' => 'px',
					),
				),
			),
			'circle_bar_style' => array(
				'title'  => __( 'Circle Styles', 'vamtam-elements-b' ),
				'fields' => array(
					'circle_width' => array(
						'type'        => 'text',
						'label'       => __( 'Circle Size', 'vamtam-elements-b' ),
						'default'     => '200',
						'maxlength'   => '4',
						'size'        => '4',
						'description' => 'px',
						'preview'  => array(
							'type'  => 'css',
							'rules' => array(
								array(
									'selector' => '.fl-countdown-number',
									'property' => 'max-width',
									'unit'     => 'px',
								),
								array(
									'selector' => '.fl-countdown-number',
									'property' => 'max-height',
									'unit'     => 'px',
								),
								array(
									'selector' => '.fl-countdown-circle-container',
									'property' => 'max-width',
									'unit'     => 'px',
								),
								array(
									'selector' => '.fl-countdown-circle-container',
									'property' => 'max-height',
									'unit'     => 'px',
								),
							),
						),

					),
					'circle_dash_width' => array(
						'type'        => 'text',
						'label'       => __( 'Circle Stroke Size', 'vamtam-elements-b' ),
						'default'     => '10',
						'maxlength'   => '2',
						'size'        => '4',
						'description' => 'px',
						'preview'  => array(
							'type'     => 'css',
							'selector' => '.svg circle',
							'property' => 'stroke-width',
							'unit'     => 'px',
						),
					),
					'circle_color' => array(
						'type'       => 'vamtam-color',
						'label'      => __( 'Circle Foreground Color', 'vamtam-elements-b' ),
						'default'    => 'accent1',
					),
					'circle_bg_color' => array(
						'type'       => 'vamtam-color',
						'label'      => __( 'Circle Background Color', 'vamtam-elements-b' ),
						'default'    => 'accent7',
					),
				),
			),
		),
	),

));
