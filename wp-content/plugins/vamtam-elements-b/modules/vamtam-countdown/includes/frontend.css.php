<?php if ( isset( $settings->number_spacing ) ) : ?>
	.fl-node-<?php echo $id; ?> .fl-countdown .fl-countdown-number {
		font-size: 1px;
		margin-left: <?php echo $settings->number_spacing ?>px;
		margin-right: <?php echo $settings->number_spacing ?>px;
	}
<?php endif; ?>

.fl-node-<?php echo $id; ?> .fl-countdown .fl-countdown-unit-label {
	<?php
		if ( ! empty( $settings->label_size ) ) {
			echo 'font-size: ' . $settings->label_size . 'px;';
		}
	?>
}

<?php if ( isset( $settings->layout ) && $settings->layout == 'default' ) : ?>
	.fl-node-<?php echo $id; ?> .fl-countdown .fl-countdown-unit {
		<?php
			if ( isset( $settings->vertical_padding ) ) {
				echo 'padding-top: ' . $settings->vertical_padding . 'px;';
				echo 'padding-bottom: ' . $settings->vertical_padding . 'px;';
			}
			if ( isset( $settings->horizontal_padding ) ) {
				echo 'padding-left: ' . $settings->horizontal_padding . 'px;';
				echo 'padding-right: ' . $settings->horizontal_padding . 'px;';
			}
			if ( isset( $settings->border_radius ) ) {
				echo 'border-radius: ' . $settings->border_radius . 'px;';
			}
		?>

		<?php if ( ! empty( $settings->number_bg_color ) ) : ?>
			background-color: <?php echo esc_html( vamtam_el_sanitize_accent( $settings->number_bg_color ) ) ?>;
		<?php endif ?>
	}

	<?php if ( $settings->show_separator == 'yes' ) : ?>
		<?php if ( $settings->separator_type == 'colon' ) : ?>
			.fl-node-<?php echo $id; ?> .fl-countdown .fl-countdown-number:after {
				<?php
					if ( isset( $settings->number_spacing ) ) {
						echo 'width: ' . ( $settings->number_spacing * 2 ) . 'px;';
						echo 'right: -' . ( $settings->number_spacing * 2 ) . 'px;';
					}
				?>

				<?php if ( isset( $settings->separator_color ) ) : ?>
					color: <?php echo esc_html( vamtam_el_sanitize_accent( $settings->separator_color ) ) ?>;
				<?php endif ?>
			}
		<?php endif; ?>

		<?php if ( $settings->separator_type == 'line' ) : ?>
			.fl-node-<?php echo $id; ?> .fl-countdown .fl-countdown-number:after {
				<?php
					if ( isset( $settings->number_spacing ) ) {
						echo 'right: -' . $settings->number_spacing . 'px;';
					}
				?>

				<?php if ( isset( $settings->separator_color ) ) : ?>
					border-color: <?php echo esc_html( vamtam_el_sanitize_accent( $settings->separator_color ) ) ?>;
				<?php endif ?>
			}
		<?php endif; ?>
	<?php endif ?>

<?php elseif ( isset( $settings->layout ) && $settings->layout == 'circle' ) : ?>
	.fl-node-<?php echo $id ?> .fl-countdown-unit {
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translate(-50%,-50%);
	}

	.fl-node-<?php echo $id ?> .fl-countdown-number{
		<?php
			if ( ! empty( $settings->circle_width ) ) {
				echo 'width: ' . $settings->circle_width . 'px;';
				echo 'height: ' . $settings->circle_width . 'px;';
			} else {
				echo 'max-width: 100px;';
				echo 'max-height: 100px;';
			}
		?>
	}
	.fl-node-<?php echo $id ?> .fl-countdown-circle-container{
		<?php
			if ( ! empty( $settings->circle_width ) ) {
				echo 'max-width: ' . $settings->circle_width . 'px;';
				echo 'max-height: ' . $settings->circle_width . 'px;';
			} else {
				echo 'max-width: 100px;';
				echo 'max-height: 100px;';
			}
		?>
	}

	.fl-node-<?php echo $id ?> .fl-countdown .svg circle{
	<?php
		if ( ! empty( $settings->circle_dash_width ) ) {
			echo 'stroke-width: ' . $settings->circle_dash_width . 'px;';
		}
	?>
	}
<?php endif; ?>
