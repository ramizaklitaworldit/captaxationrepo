(function($) {

	FLBuilderTabs = function( settings ) {
		this.settings  = settings;
		this.nodeClass = '.fl-node-' + settings.id;
		this._init();
	};

	FLBuilderTabs.prototype = {

		settings	: {},
		nodeClass   : '',

		_init: function() {
			var win = $( window );

			document.querySelector( this.nodeClass + ' .fl-tabs-labels' ).addEventListener( 'click', this._labelClick.bind( this ) );

			if ( $( this.nodeClass + ' .fl-tabs-vertical' ).length > 0 ) {
				this._resize();
				win.off( 'resize' + this.nodeClass );
				win.on( 'resize' + this.nodeClass, this._resize.bind( this ) );
			}

			FLBuilderLayout.preloadAudio( this.nodeClass + ' .fl-tabs-panel-content' );
		},

		_labelClick: function( e ) {
			var label = $( e.target ).closest( '.fl-tabs-label' );

			if ( ! label ) {
				return;
			}

			var index    = label.data( 'index' ),
				wrap     = label.closest( '.fl-tabs' ),
				allIcons = wrap.find( '.fl-tabs-panels .fl-tabs-label .fa' ),
				icon     = wrap.find( '.fl-tabs-panels .fl-tabs-label[data-index="' + index + '"] .fa' );

			// Toggle the responsive icons.
			allIcons.addClass( 'fa-plus' );
			icon.removeClass( 'fa-plus' );

			// Toggle the tabs.
			wrap.find( '.fl-tabs-labels:first > .fl-tab-active' ).removeClass( 'fl-tab-active' );
			wrap.find( '.fl-tabs-panels:first > .fl-tabs-panel > .fl-tab-active' ).removeClass( 'fl-tab-active' );

			wrap.find( '.fl-tabs-labels:first > .fl-tabs-label[data-index="' + index + '"]' ).addClass( 'fl-tab-active' );
			wrap.find( '.fl-tabs-panels:first > .fl-tabs-panel > .fl-tabs-panel-content[data-index="' + index + '"]' ).addClass( 'fl-tab-active' );

			// Gallery module support.
			FLBuilderLayout.refreshGalleries( wrap.find( '.fl-tabs-panel-content[data-index="' + index + '"]' ) );

			// Grid layout support (uses Masonry)
			FLBuilderLayout.refreshGridLayout( wrap.find( '.fl-tabs-panel-content[data-index="' + index + '"]' ) );

			// Post Carousel support (uses BxSlider)
			FLBuilderLayout.reloadSlider( wrap.find( '.fl-tabs-panel-content[data-index="' + index + '"]' ) );

			// WP audio shortcode support
			FLBuilderLayout.resizeAudio( wrap.find( '.fl-tabs-panel-content[data-index="' + index + '"]' ) );
		},

		_resize: function() {
			$( this.nodeClass + ' .fl-tabs-vertical' ).each( this._resizeVertical.bind( this ) );
		},

		_resizeVertical: function( e ) {
			var wrap   = $( this.nodeClass + ' .fl-tabs-vertical' ),
				labels = wrap.find( '.fl-tabs-labels' ),
				panels = wrap.find( '.fl-tabs-panels' );

			panels.css( 'min-height', labels.height() + 'px' );
		}
	};

})(jQuery);
