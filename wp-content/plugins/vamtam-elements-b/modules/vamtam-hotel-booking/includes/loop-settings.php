<?php

FLBuilderModel::default_settings($settings, array(
	'post_type' => 'hb_room',
	'order_by'  => 'date',
	'order'     => 'DESC',
	'offset'    => 0,
	'users'     => '',
));

?>
<div id="fl-builder-settings-section-general" class="fl-loop-builder fl-builder-settings-section">

	<table class="fl-form-table">
		<?php
		// Order by
		FLBuilder::render_settings_field('order_by', array(
			'type'    => 'select',
			'label'   => esc_html__( 'Order By', 'fl-builder' ),
			'options' => array(
				'ID'            => esc_html__( 'ID', 'fl-builder' ),
				'date'          => esc_html__( 'Date', 'fl-builder' ),
				'modified'      => esc_html__( 'Date Last Modified', 'fl-builder' ),
				'title'         => esc_html__( 'Title', 'fl-builder' ),
				'author'        => esc_html__( 'Author', 'fl-builder' ),
				'comment_count' => esc_html__( 'Comment Count', 'fl-builder' ),
				'menu_order'    => esc_html__( 'Menu Order', 'fl-builder' ),
				'rand'          => esc_html__( 'Random', 'fl-builder' ),
			),
		), $settings);

		// Order
		FLBuilder::render_settings_field('order', array(
			'type'    => 'select',
			'label'   => esc_html__( 'Order', 'fl-builder' ),
			'options' => array(
				'DESC' => esc_html__( 'Descending', 'fl-builder' ),
				'ASC'  => esc_html__( 'Ascending', 'fl-builder' ),
			),
		), $settings);

		// Offset
		FLBuilder::render_settings_field('offset', array(
			'type'    => 'text',
			'label'   => esc_html_x( 'Offset', 'How many posts to skip.', 'fl-builder' ),
			'default' => '0',
			'size'    => '4',
			'help'    => esc_html__( 'Skip this many posts that match the specified criteria.', 'fl-builder' ),
		), $settings);

		?>
	</table>
</div>
<div id="fl-builder-settings-section-filter" class="fl-builder-settings-section">
	<h3 class="fl-builder-settings-title"><?php esc_html_e( 'Filter', 'fl-builder' ); ?></h3>
	<?php
		$slug = 'hb_room';
		$type = get_post_type_object( $slug );
	?>
	<table class="fl-form-table fl-loop-builder-filter fl-loop-builder-<?php echo esc_attr( $slug ) ?>-filter" <?php if ($slug == $settings->post_type) echo 'style="display:table;"'; ?>>
		<?php

		// Posts
		FLBuilder::render_settings_field('posts_' . $slug, array(
			'type'   => 'suggest',
			'action' => 'fl_as_posts',
			'data'   => $slug,
			'label'  => $type->label,
			'help'   => sprintf( esc_html__( 'Enter a comma separated list of %1$s. Only these %2$s will be shown.', 'fl-builder' ), $type->label, $type->label ),
		), $settings);

		// Taxonomies
		$taxonomies = FLBuilderLoop::taxonomies( $slug );

		foreach ( $taxonomies as $tax_slug => $tax ) {

			FLBuilder::render_settings_field('tax_' . $slug . '_' . $tax_slug, array(
				'type'   => 'suggest',
				'action' => 'fl_as_terms',
				'data'   => $tax_slug,
				'label'  => $tax->label,
				'help'   => sprintf( esc_html__( 'Enter a comma separated list of %1$s. Only posts with these %2$s will be shown.', 'fl-builder' ), $tax->label, $tax->label ),
			), $settings);
		}

		?>
	</table>
	<table class="fl-form-table">
		<?php

		// Author
		FLBuilder::render_settings_field('users', array(
			'type'   => 'suggest',
			'action' => 'fl_as_users',
			'label'  => esc_html__( 'Authors', 'fl-builder' ),
			'help'   => esc_html__( 'Enter a comma separated list of authors usernames. Only posts with these authors will be shown.', 'fl-builder' ),
		), $settings);

		?>
	</table>
</div>
