<?php

$has_lightbox = vamtam_sanitize_bool( $settings->lightbox_embed );

$button_class = array(
	'vamtam-button',
	$settings->color,
	'hover-' . $settings->hover_color,
	'button-' . $settings->layout_type,
	'icon-animation-' . $settings->icon_animation,
);

$button_attrs = '';

if ( $has_lightbox ) {
	global $wp_embed, $content_width;

	$lightbox_content = do_shortcode( $wp_embed->run_shortcode( '[embed width="' . esc_attr( $content_width ) . '"]' . $settings->link . '[/embed]' ) );

	$button_attrs .= ' data-vamtam-lightbox="' . esc_attr( $lightbox_content ) . '"';

	$settings->link_target = '_blank';

	Vamtam_Elements_B::enqueue_lightbox_template();
}

$style = 'font-size:' . (int) $settings->font_size . 'px;line-height:' . ((int) $settings->font_size + 2 ) . 'px;';

if ( 'custom' == $settings->width ) {
	$style .= 'width:' . (int) $settings->custom_width . 'px;';
}

if ( ! empty( $settings->padding ) ) {
	if ( 'underline' === $settings->layout_type ) {
		$style .= 'padding:' . (int) $settings->padding . 'px 0px;';
	} else {
		$style .= 'padding:' . (int) $settings->padding . 'px ' . (int) $settings->padding * 2 . 'px;';
	}
}

$button_attrs .= 'style="' . esc_attr( $style ) . '"';

$icon_content = '';

if ( function_exists( 'vamtam_get_icon' ) && strpos( $settings->icon, 'vamtam-theme-' ) === 0 ) {
	$icon_content = 'data-icon="' . esc_attr( vamtam_get_icon( $settings->icon ) ) . '"';
	$settings->icon = 'icon icon-b theme';
}

?>
<div class="<?php echo esc_attr( $module->get_classname() ) ?>" style="<?php if ( ! empty( $settings->align ) ) echo esc_attr( 'text-align:' . $settings->align ) ?>">
	<a href="<?php echo esc_url( $settings->link ) ?>" target="<?php echo esc_attr( $settings->link_target ) ?>" class="<?php echo esc_attr( implode( ' ', $button_class ) ) ?>" role="button" <?php echo $button_attrs // xss ok ?>>

		<?php if ( ! empty( $settings->icon ) && ( 'before' == $settings->icon_position || ! isset( $settings->icon_position ) ) ) : ?>
			<i class="vamtam-button-icon vamtam-button-icon-before <?php echo esc_attr( $settings->icon ) ?>" <?php if ( empty( $settings->text ) ) : ?>style="margin-right:0"<?php endif ?> <?php echo $icon_content // xss ok ?>></i>
		<?php endif; ?>

		<?php if ( ! empty( $settings->text ) ) : ?>
			<span class="vamtam-button-text"><?php echo $settings->text; // xss ok ?></span>
		<?php endif; ?>

		<?php if ( ! empty( $settings->icon ) && 'after' == $settings->icon_position ) : ?>
			<i class="vamtam-button-icon vamtam-button-icon-after <?php echo esc_attr( $settings->icon ) ?>" <?php if ( empty( $settings->text ) ) : ?>style="margin-left:0"<?php endif ?> <?php echo $icon_content // xss ok ?>></i>
		<?php endif; ?>

	</a>
</div>
