<?php

/**
 * @class VamtamButtonModule
 */
class VamtamButtonModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct() {
		$path = trailingslashit( 'modules/' . basename( dirname( __FILE__ ) ) );

		parent::__construct(array(
			'name'            => __( 'Button', 'vamtam-elements-b' ),
			'description'     => __( 'A simple call to action button.', 'vamtam-elements-b' ),
			'category'        => __( 'VamTam Modules', 'vamtam-elements-b' ),
			'partial_refresh' => true,
			'dir'             => VAMTAMEL_B_DIR . $path,
			'url'             => VAMTAMEL_B_URL . $path,
		));
	}

	/**
	 * @method update
	 */
	public function update( $settings ) {

		// Remove the old three_d setting.
		if ( isset( $settings->three_d ) ) {
			unset( $settings->three_d );
		}

		return $settings;
	}

	/**
	 * @method get_classname
	 */
	public function get_classname() {

		$classname = 'vamtam-button-wrap';

		if ( ! empty( $this->settings->width ) ) {
			$classname .= ' vamtam-button-width-' . $this->settings->width;
		}
		if ( ! empty( $this->settings->icon ) ) {
			$classname .= ' vamtam-button-has-icon';
		}

		return $classname;
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('VamtamButtonModule', array(
	'vamtam-button' => array(
		'title'    => __( 'General', 'vamtam-elements-b' ),
		'sections' => array(
			'general' => array(
				'title'  => '',
				'fields' => array(
					'text' => array(
						'type'    => 'text',
						'label'   => __( 'Text', 'vamtam-elements-b' ),
						'default' => __( 'Click Here', 'vamtam-elements-b' ),
						'preview' => array(
							'type'     => 'text',
							'selector' => '.vamtam-button-text',
						),
						'vamtam-wpml' => 'LINE',
					),
					'icon' => array(
						'type'        => 'icon',
						'label'       => __( 'Icon', 'vamtam-elements-b' ),
						'show_remove' => true,
					),
					'icon_position' => array(
						'type'    => 'select',
						'label'   => __( 'Icon Position', 'vamtam-elements-b' ),
						'default' => 'before',
						'options' => array(
							'before' => __( 'Before Text', 'vamtam-elements-b' ),
							'after'  => __( 'After Text', 'vamtam-elements-b' ),
						),
					),
					'icon_animation' => array(
						'type'    => 'select',
						'label'   => __( 'Icon Visibility', 'vamtam-elements-b' ),
						'default' => 'disable',
						'options' => array(
							'disable' => __( 'Always Visible', 'vamtam-elements-b' ),
							'enable'  => __( 'Fade In On Hover', 'vamtam-elements-b' ),
						),
					),
				),
			),
			'link' => array(
				'title'  => __( 'Link', 'vamtam-elements-b' ),
				'fields' => array(
					'link' => array(
						'type'        => 'link',
						'label'       => __( 'Link', 'vamtam-elements-b' ),
						'placeholder' => __( 'http://www.example.com', 'vamtam-elements-b' ),
						'preview'     => array(
							'type' => 'none',
						),
					),
					'lightbox_embed' => array(
						'type'    => 'select',
						'label'   => __( 'Display contents in a lightbox (oEmbed required)', 'vamtam-elements-b' ),
						'default' => 'false',
						'options' => array(
							'false' => __( 'No, treat as regular link', 'vamtam-elements-b' ),
							'true'  => __( 'Use lightbox', 'vamtam-elements-b' ),
						),
						'preview' => array(
							'type' => 'none',
						),
						'toggle' => array(
							'false' => array(
								'fields' => array( 'link_target' ),
							),
						),
					),
					'link_target' => array(
						'type'    => 'select',
						'label'   => __( 'Link Target', 'vamtam-elements-b' ),
						'default' => '_self',
						'options' => array(
							'_self'  => __( 'Same Window', 'vamtam-elements-b' ),
							'_blank' => __( 'New Window', 'vamtam-elements-b' ),
						),
						'preview' => array(
							'type' => 'none',
						),
					),
				),
			),
		),
	),
	'style' => array(
		'title'    => __( 'Style', 'vamtam-elements-b' ),
		'sections' => array(
			'layout' => array(
				'title'  => '',
				'fields' => array(
					'layout_type' => array(
						'type'    => 'select',
						'label'   => __( 'Type', 'vamtam-elements-b' ),
						'default' => 'solid',
						'options' => array(
							'solid'     => esc_html__( 'Solid', 'vamtam-elements-b' ),
							'border'    => esc_html__( 'Border', 'vamtam-elements-b' ),
							'underline' => esc_html__( 'Underline', 'vamtam-elements-b' ),
						),
					),
				),
			),
			'colors' => array(
				'title'  => __( 'Colors', 'vamtam-elements-b' ),
				'fields' => array(
					'color' => array(
						'type'    => 'select',
						'label'   => __( 'Normal Color', 'vamtam-elements-b' ),
						'default' => 'accent1',
						'options' => array(
							'accent1' => esc_html__( 'Accent 1', 'vamtam-elements-b' ),
							'accent2' => esc_html__( 'Accent 2', 'vamtam-elements-b' ),
							'accent3' => esc_html__( 'Accent 3', 'vamtam-elements-b' ),
							'accent4' => esc_html__( 'Accent 4', 'vamtam-elements-b' ),
							'accent5' => esc_html__( 'Accent 5', 'vamtam-elements-b' ),
							'accent6' => esc_html__( 'Accent 6', 'vamtam-elements-b' ),
							'accent7' => esc_html__( 'Accent 7', 'vamtam-elements-b' ),
							'accent8' => esc_html__( 'Accent 8', 'vamtam-elements-b' ),
						),
					),
					'hover_color' => array(
						'type'    => 'select',
						'label'   => __( 'Hover Color', 'vamtam-elements-b' ),
						'default' => 'accent6',
						'options' => array(
							'accent1' => esc_html__( 'Accent 1', 'vamtam-elements-b' ),
							'accent2' => esc_html__( 'Accent 2', 'vamtam-elements-b' ),
							'accent3' => esc_html__( 'Accent 3', 'vamtam-elements-b' ),
							'accent4' => esc_html__( 'Accent 4', 'vamtam-elements-b' ),
							'accent5' => esc_html__( 'Accent 5', 'vamtam-elements-b' ),
							'accent6' => esc_html__( 'Accent 6', 'vamtam-elements-b' ),
							'accent7' => esc_html__( 'Accent 7', 'vamtam-elements-b' ),
							'accent8' => esc_html__( 'Accent 8', 'vamtam-elements-b' ),
						),
					),
				),
			),
			'formatting' => array(
				'title'  => __( 'Structure', 'vamtam-elements-b' ),
				'fields' => array(
					'width' => array(
						'type'    => 'select',
						'label'   => __( 'Width', 'vamtam-elements-b' ),
						'default' => 'auto',
						'options' => array(
							'auto'   => _x( 'Auto', 'Width.', 'vamtam-elements-b' ),
							'full'   => __( 'Full Width', 'vamtam-elements-b' ),
							'custom' => __( 'Custom', 'vamtam-elements-b' ),
						),
						'toggle' => array(
							'auto' => array(
								'fields' => array( 'align' ),
							),
							'full'   => array(),
							'custom' => array(
								'fields' => array( 'align', 'custom_width' ),
							),
						),
					),
					'custom_width' => array(
						'type'        => 'text',
						'label'       => __( 'Custom Width', 'vamtam-elements-b' ),
						'default'     => '200',
						'maxlength'   => '3',
						'size'        => '4',
						'description' => 'px',
					),
					'align' => array(
						'type'    => 'select',
						'label'   => __( 'Alignment', 'vamtam-elements-b' ),
						'default' => 'left',
						'options' => array(
							'center' => __( 'Center', 'vamtam-elements-b' ),
							'left'   => __( 'Left', 'vamtam-elements-b' ),
							'right'  => __( 'Right', 'vamtam-elements-b' ),
						),
					),
					'font_size' => array(
						'type'        => 'text',
						'label'       => __( 'Font Size', 'vamtam-elements-b' ),
						'default'     => '14',
						'maxlength'   => '3',
						'size'        => '4',
						'description' => 'px',
					),
					'padding' => array(
						'type'        => 'text',
						'label'       => __( 'Padding', 'vamtam-elements-b' ),
						'default'     => '16',
						'maxlength'   => '3',
						'size'        => '4',
						'description' => 'px',
					),
				),
			),
		),
	),
));
