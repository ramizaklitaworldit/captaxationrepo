<?php

class VamtamIframeModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct() {
		$path = trailingslashit( 'modules/' . basename( dirname( __FILE__ ) ) );

		parent::__construct(array(
			'name'            => esc_html__( 'Iframe', 'vamtam-elements-b' ),
			'description'     => esc_html__( 'Display a <iframe> without blocking the load event.', 'vamtam-elements-b' ),
			'category'        => esc_html__( 'VamTam Modules', 'vamtam-elements-b' ),
			'partial_refresh' => true,
			'dir'             => VAMTAMEL_B_DIR . $path,
			'url'             => VAMTAMEL_B_URL . $path,
		));
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module( 'VamtamIframeModule', array(
	'general' => array(
		'title'    => __( 'General', 'vamtam-elements-b' ),
		'sections' => array(
			'general' => array(
				'title'  => '',
				'fields' => array(
					'src' => array(
						'type'        => 'link',
						'label'       => esc_html__( 'Link', 'vamtam-elements-b' ),
						'placeholder' => 'https://example.com',
					),
					'height' => array(
						'type'        => 'unit',
						'label'       => esc_html__( 'Height', 'vamtam-elements-b' ),
						'default'     => '400',
						'min'         => '1',
						'description' => 'px',
					),
					'width' => array(
						'type'        => 'text',
						'label'       => esc_html__( 'Width', 'vamtam-elements-b' ),
						'default'     => '100%',
						'size'        => 3,
						'description' => esc_html__( 'Enter a value and its unit, for example: 500px or 100%', 'vamtam-elements-b' ),
					),
				),
			),
		),
	),
));
