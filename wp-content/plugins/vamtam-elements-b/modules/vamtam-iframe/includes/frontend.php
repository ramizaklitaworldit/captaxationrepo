<div class="vamtam-lazy-iframe">
	<?php $attribute = FLBuilderModel::is_builder_active() ? 'src' : 'src="about:blank" data-src' ?>
	<iframe <?= $attribute //xss ok ?>="<?php echo esc_url( $settings->src ) ?>" height="<?php echo (int)$settings->height; ?>" frameborder="0" style="border:0;width:<?php echo esc_attr( $settings->width ) ?>"></iframe>
</div>
