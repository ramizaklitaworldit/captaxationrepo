<?php

/**
 * A module that adds a simple subscribe form to your layout
 * with third party optin integration.
 *
 * @since 1.5.2
 */
class VamtamSubscribeFormModule extends FLBuilderModule {

	/**
	 * @since 1.5.2
	 * @return void
	 */
	public function __construct() {
		$path = trailingslashit( 'modules/' . basename( dirname( __FILE__ ) ) );

		parent::__construct( array(
			'name'            => __( 'Subscribe Form', 'vamtam-elements-b' ),
			'description'     => __( 'Adds a simple subscribe form to your layout.', 'vamtam-elements-b' ),
			'category'        => __( 'VamTam Modules', 'vamtam-elements-b' ),
			'editor_export'   => false,
			'partial_refresh' => true,
			'dir'             => VAMTAMEL_B_DIR . $path,
			'url'             => VAMTAMEL_B_URL . $path,
		));

		add_action( 'wp_ajax_fl_builder_subscribe_form_submit', array( $this, 'submit' ) );
		add_action( 'wp_ajax_nopriv_fl_builder_subscribe_form_submit', array( $this, 'submit' ) );
	}

	public function enqueue_scripts() {
		$this->add_js( 'vamtam-fl-subscribe-form' );
	}

	public static function get_button_settings( $settings ) {
		return array(
			'align'         => '',
			'layout_type'   => $settings->btn_layout_type,
			'color'         => $settings->btn_color,
			'hover_color'   => $settings->btn_hover_color,
			'font_size'     => $settings->btn_font_size,
			'icon'          => $settings->btn_icon,
			'icon_position' => $settings->btn_icon_position,
			'link'          => '#',
			'link_target'   => '_self',
			'padding'       => $settings->btn_padding,
			'text'          => $settings->btn_text,
			'width'         => 'auto',
		);
	}

	/**
	 * Called via AJAX to submit the subscribe form.
	 *
	 * @since 1.5.2
	 * @return string The JSON encoded response.
	 */
	public function submit() {

		$name             = isset( $_POST['name'] ) ? sanitize_text_field( $_POST['name'] ) : false;
		$email            = isset( $_POST['email'] ) ? sanitize_email( $_POST['email'] ) : false;
		$node_id          = isset( $_POST['node_id'] ) ? sanitize_text_field( $_POST['node_id'] ) : false;
		$template_id      = isset( $_POST['template_id'] ) ? sanitize_text_field( $_POST['template_id'] ) : false;
		$template_node_id = isset( $_POST['template_node_id'] ) ? sanitize_text_field( $_POST['template_node_id'] ) : false;
		$result           = array(
			'action'  => false,
			'error'   => false,
			'message' => false,
			'url'     => false,
		);

		if ( $email && $node_id ) {

			// Get the module settings.
			if ( $template_id ) {
				$post_id  = FLBuilderModel::get_node_template_post_id( $template_id );
				$data     = FLBuilderModel::get_layout_data( 'published', $post_id );
				$settings = $data[ $template_node_id ]->settings;
			} else {
				$module   = FLBuilderModel::get_module( $node_id );
				$settings = $module->settings;
			}

			// Subscribe.
			$instance = FLBuilderServices::get_service_instance( $settings->service );
			$response = $instance->subscribe( $settings, $email, $name );

			// Check for an error from the service.
			if ( $response['error'] ) {
				$result['error'] = $response['error'];
			}
			else {

				$result['action'] = $settings->success_action;

				if ( 'message' == $settings->success_action ) {
					$result['message'] = $settings->success_message;
				} else {
					$result['url'] = $settings->success_url;
				}
			}
		} else {
			$result['error'] = __( 'There was an error subscribing. Please try again.', 'vamtam-elements-b' );
		}

		echo json_encode( $result );

		die();
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module( 'VamtamSubscribeFormModule', array(
	'general' => array(
		'title'    => __( 'General', 'vamtam-elements-b' ),
		'sections' => array(
			'service' => array(
				'title'    => '',
				'file'     => FL_BUILDER_DIR . 'includes/service-settings.php',
				'services' => 'autoresponder',
			),
			'structure' => array(
				'title'  => __( 'Structure', 'vamtam-elements-b' ),
				'fields' => array(
					'layout' => array(
						'type'    => 'select',
						'label'   => __( 'Layout', 'vamtam-elements-b' ),
						'default' => 'stacked',
						'options' => array(
							'stacked' => __( 'Stacked', 'vamtam-elements-b' ),
							'inline'  => __( 'Inline', 'vamtam-elements-b' ),
						),
					),
					'show_name' => array(
						'type'    => 'select',
						'label'   => __( 'Name Field', 'vamtam-elements-b' ),
						'default' => 'show',
						'options' => array(
							'show' => __( 'Show', 'vamtam-elements-b' ),
							'hide' => __( 'Hide', 'vamtam-elements-b' ),
						),
					),
				),
			),
			'success' => array(
				'title'  => __( 'Success', 'vamtam-elements-b' ),
				'fields' => array(
					'success_action' => array(
						'type'    => 'select',
						'label'   => __( 'Success Action', 'vamtam-elements-b' ),
						'options' => array(
							'message'  => __( 'Show Message', 'vamtam-elements-b' ),
							'redirect' => __( 'Redirect', 'vamtam-elements-b' ),
						),
						'toggle' => array(
							'message' => array(
								'fields' => array( 'success_message' ),
							),
							'redirect' => array(
								'fields' => array( 'success_url' ),
							),
						),
						'preview' => array(
							'type' => 'none',
						),
					),
					'success_message' => array(
						'type'          => 'editor',
						'label'         => '',
						'media_buttons' => false,
						'rows'          => 8,
						'default'       => __( 'Thanks for subscribing! Please check your email for further instructions.', 'vamtam-elements-b' ),
						'preview'       => array(
							'type' => 'none',
						),
					),
					'success_url' => array(
						'type'    => 'link',
						'label'   => __( 'Success URL', 'vamtam-elements-b' ),
						'preview' => array(
							'type' => 'none',
						),
					),
				),
			),
		),
	),
	'button' => array(
		'title'    => __( 'Button', 'vamtam-elements-b' ),
		'sections' => array(
			'btn_general' => array(
				'title'  => '',
				'fields' => array(
					'btn_text' => array(
						'type'        => 'text',
						'label'       => __( 'Button Text', 'vamtam-elements-b' ),
						'default'     => __( 'Subscribe!', 'vamtam-elements-b' ),
						'vamtam-wpml' => 'line',
					),
					'btn_icon' => array(
						'type'        => 'icon',
						'label'       => __( 'Button Icon', 'vamtam-elements-b' ),
						'show_remove' => true,
					),
					'btn_icon_position' => array(
						'type'    => 'select',
						'label'   => __( 'Icon Position', 'vamtam-elements-b' ),
						'default' => 'before',
						'options' => array(
							'before' => __( 'Before Text', 'vamtam-elements-b' ),
							'after'  => __( 'After Text', 'vamtam-elements-b' ),
						),
					),
				),
			),
			'btn_colors' => array(
				'title'  => __( 'Button Colors', 'vamtam-elements-b' ),
				'fields' => array(
					'btn_color' => array(
						'type'    => 'select',
						'label'   => __( 'Normal Color', 'vamtam-elements-b' ),
						'default' => 'accent1',
						'options' => array(
							'accent1' => esc_html__( 'Accent 1', 'vamtam-elements-b' ),
							'accent2' => esc_html__( 'Accent 2', 'vamtam-elements-b' ),
							'accent3' => esc_html__( 'Accent 3', 'vamtam-elements-b' ),
							'accent4' => esc_html__( 'Accent 4', 'vamtam-elements-b' ),
							'accent5' => esc_html__( 'Accent 5', 'vamtam-elements-b' ),
							'accent6' => esc_html__( 'Accent 6', 'vamtam-elements-b' ),
							'accent7' => esc_html__( 'Accent 7', 'vamtam-elements-b' ),
							'accent8' => esc_html__( 'Accent 8', 'vamtam-elements-b' ),
						),
					),
					'btn_hover_color' => array(
						'type'    => 'select',
						'label'   => __( 'Hover Color', 'vamtam-elements-b' ),
						'default' => 'accent2',
						'options' => array(
							'accent1' => esc_html__( 'Accent 1', 'vamtam-elements-b' ),
							'accent2' => esc_html__( 'Accent 2', 'vamtam-elements-b' ),
							'accent3' => esc_html__( 'Accent 3', 'vamtam-elements-b' ),
							'accent4' => esc_html__( 'Accent 4', 'vamtam-elements-b' ),
							'accent5' => esc_html__( 'Accent 5', 'vamtam-elements-b' ),
							'accent6' => esc_html__( 'Accent 6', 'vamtam-elements-b' ),
							'accent7' => esc_html__( 'Accent 7', 'vamtam-elements-b' ),
							'accent8' => esc_html__( 'Accent 8', 'vamtam-elements-b' ),
						),
					),
				),
			),
			'btn_style' => array(
				'title'  => __( 'Button Style', 'vamtam-elements-b' ),
				'fields' => array(
					'btn_layout_type' => array(
						'type'    => 'select',
						'label'   => __( 'Button Type', 'vamtam-elements-b' ),
						'default' => 'solid',
						'options' => array(
							'solid'     => esc_html__( 'Solid', 'vamtam-elements-b' ),
							'border'    => esc_html__( 'Border', 'vamtam-elements-b' ),
							'underline' => esc_html__( 'Underline', 'vamtam-elements-b' ),
						),
					),
				),
			),
			'btn_structure' => array(
				'title'  => __( 'Button Structure', 'vamtam-elements-b' ),
				'fields' => array(
					'btn_width' => array(
						'type'    => 'select',
						'label'   => __( 'Width', 'vamtam-elements-b' ),
						'default' => 'full',
						'options' => array(
							'auto' => _x( 'Auto', 'Width.', 'vamtam-elements-b' ),
							'full' => __( 'Full Width', 'vamtam-elements-b' ),
						),
					),
					'btn_font_size' => array(
						'type'        => 'text',
						'label'       => __( 'Font Size', 'vamtam-elements-b' ),
						'default'     => '16',
						'maxlength'   => '3',
						'size'        => '4',
						'description' => 'px',
					),
					'btn_padding' => array(
						'type'        => 'text',
						'label'       => __( 'Padding', 'vamtam-elements-b' ),
						'default'     => '12',
						'maxlength'   => '3',
						'size'        => '4',
						'description' => 'px',
					),
				),
			),
		),
	),
));
