.fl-node-<?php echo $id; ?> .fl-subscribe-form .fl-form-field input,
.fl-node-<?php echo $id; ?> .fl-subscribe-form .fl-form-field input[type=text] {
	font-size: <?php echo (int)$settings->btn_font_size; ?>px;
	line-height: <?php echo (int)$settings->btn_font_size + 2; ?>px;
	padding: <?php echo (int)$settings->btn_padding . 'px ' . ( (int)$settings->btn_padding * 2 ) . 'px'; ?>;
}
.fl-node-<?php echo $id; ?> .fl-subscribe-form-inline .fl-form-field input,
.fl-node-<?php echo $id; ?> .fl-subscribe-form-inline .fl-form-field input[type=text],
.fl-node-<?php echo $id; ?> .fl-subscribe-form-inline a.vamtam-button {
	height: <?php echo ( (int)$settings->btn_padding * 2 ) + ( (int)$settings->btn_font_size + 2 ); ?>px;
}
