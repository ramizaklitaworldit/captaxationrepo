<?php if ( ! empty( $settings->number_size ) || ! empty( $settings->number_color ) ) : ?>
	.fl-node-<?php echo $id ?> .fl-number-string {
	<?php
		if ( ! empty( $settings->number_size ) ) {
			echo 'font-size: ' . $settings->number_size . 'px;';
		}
	?>
	}
<?php endif; ?>

<?php if ( ! empty( $settings->text_color ) ) : ?>
	.fl-node-<?php echo esc_html( $id ) ?> .fl-module-content .fl-number-before-text,
	.fl-node-<?php echo esc_html( $id ) ?> .fl-module-content .fl-number-after-text {
		color: <?php echo esc_html( vamtam_el_sanitize_accent( $settings->text_color ) ) ?>;
	}
<?php endif; ?>

<?php if ( isset( $settings->layout ) && $settings->layout === 'default' ) : ?>
	.fl-node-<?php echo $id ?> .fl-number-text {
		text-align: <?php echo esc_html( $settings->text_align ) ?>;
	}
<?php endif ?>

<?php if ( isset( $settings->layout ) && $settings->layout === 'circle' ) : ?>
	.fl-node-<?php echo $id ?> .fl-number-text {
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translate(-50%,-50%);
	}

	.fl-node-<?php echo $id ?> .fl-number-circle-container {
		<?php
			if ( ! empty( $settings->circle_width ) ) {
				echo 'max-width: ' . $settings->circle_width . 'px;';
				echo 'max-height: ' . $settings->circle_width . 'px;';
			} else {
				echo 'max-width: 100px;';
				echo 'max-height: 100px;';
			}
		?>
	}

	.fl-node-<?php echo $id ?> .svg circle{
	<?php
		if ( ! empty( $settings->circle_dash_width ) ) {
			echo 'stroke-width: ' . $settings->circle_dash_width . 'px;';
		}
	?>
	}
<?php endif; ?>
