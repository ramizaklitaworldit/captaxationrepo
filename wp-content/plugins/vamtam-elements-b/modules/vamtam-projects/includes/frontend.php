<?php

if ( ! class_exists( 'Jetpack_Portfolio' ) ) {
	return 'Jetpack_Portfolio not found.';
}

$settings->show_title = vamtam_sanitize_bool( $settings->show_title );

$settings->title_filter = vamtam_sanitize_bool( $settings->title_filter );
$settings->type_filter  = vamtam_sanitize_bool( $settings->type_filter );

$settings->description = vamtam_sanitize_bool( $settings->description );

$settings->gap = vamtam_sanitize_bool( $settings->gap );

$scrollable = 'scrollable' === $settings->layout;

if ( $scrollable ) {
	$settings->pagination         = 'false';
	$settings->image_aspect_ratio = 'fixed';
}

$settings->columns = (int) $settings->columns;

$old_column                         = isset( $GLOBALS['vamtam_portfolio_column'] ) ? $GLOBALS['vamtam_portfolio_column'] : null;
$GLOBALS['vamtam_portfolio_column'] = $settings->columns;

$max_columns = $settings->columns;

if ( 0 === $settings->columns ) {
	$settings->columns = 4; // this is used for thumbnails only
}

wp_reset_query();

// Get the query data.
FLBuilderModel::default_settings( $settings, array(
	'post_type' => Jetpack_Portfolio::CUSTOM_POST_TYPE,
) );

if ( isset( $settings->offset ) && ! $settings->offset ) {
	unset( $settings->offset );
}

$settings = apply_filters( 'vamtam_projects_query_settings', $settings );

if (
	is_post_type_archive( Jetpack_Portfolio::CUSTOM_POST_TYPE ) ||
	is_tax( [ Jetpack_Portfolio::CUSTOM_TAXONOMY_TYPE, Jetpack_Portfolio::CUSTOM_TAXONOMY_TAG ] )
) {
	$portfolio_query = $GLOBALS['wp_query'];
} else {
	$portfolio_query = FLBuilderLoop::query( $settings );
}

if ( $scrollable ) {
	include locate_template( 'templates/portfolio/scrollable.php' );
} else {
	include locate_template( 'templates/portfolio/loop.php' );
}

// Render the empty message.
if ( ! $portfolio_query->have_posts() && (defined( 'DOING_AJAX' ) || isset( $_REQUEST['fl_builder'] )) ) :

?>
<div class="fl-post-grid-empty">
	<?php esc_html_e( 'No projects found.', 'vamtam-elements-b' ); ?>
</div>

<?php

endif;

wp_reset_postdata();

$GLOBALS['vamtam_portfolio_column'] = $old_column;

wp_reset_query();
