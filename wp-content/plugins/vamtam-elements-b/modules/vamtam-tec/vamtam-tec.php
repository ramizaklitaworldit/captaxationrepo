<?php

class VamtamTecModule extends FLBuilderModule {

	public function __construct() {
		$path = trailingslashit( 'modules/' . basename( dirname( __FILE__ ) ) );

		parent::__construct(array(
			'name'            => esc_html__( 'The Events Calendar', 'vamtam-elements-b' ),
			'description'     => '',
			'category'        => esc_html__( 'VamTam Modules', 'vamtam-elements-b' ),
			'partial_refresh' => true,
			'dir'             => VAMTAMEL_B_DIR . $path,
			'url'             => VAMTAMEL_B_URL . $path,
			'enabled'         => function_exists( 'tribe_get_events' ) && current_theme_supports( 'vamtam-tribe-events' ),
		));
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module( 'VamtamTecModule', array(
	'layout' => array(
		'title'    => esc_html__( 'General', 'vamtam-elements-b' ),
		'sections' => array(
			'general' => array(
				'title'  => '',
				'fields' => array(
					'posts_per_page' => array(
						'label'   => esc_html__( 'Number of Events', 'vamtam-elements-b' ),
						'type'    => 'unit',
						'min'     => 1,
						'max'     => 100,
						'default' => 4,
					),

					'columns' => array(
						'label'   => esc_html__( 'Columns', 'vamtam-elements-b' ),
						'type'    => 'unit',
						'min'     => 1,
						'max'     => 4,
						'default' => 4,
					),

					'category' => array(
						'label'  => esc_html__( 'Categories (optional)', 'vamtam-elements-b' ),
						'desc'   => esc_html__( 'All categories will be shown if none are selected. Please note that if you do not see categories, there are none created most probably. You can use ctr + click to select multiple categories.', 'vamtam-elements-b' ),
						'data'   => 'tribe_events_cat',
						'action' => 'fl_as_terms',
						'type'   => 'suggest',
					),
				),
			),
		),
	),
) );

