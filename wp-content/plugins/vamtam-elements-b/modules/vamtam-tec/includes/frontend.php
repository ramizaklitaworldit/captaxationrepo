<?php

if ( ! function_exists( 'tribe_get_events' ) ) {
	return '';
}

$query = array(
	'posts_per_page' => $settings->posts_per_page,
	'order'          => 'ASC',
	'start_date'     => tribe_format_date( current_time( 'timestamp' ), true, 'Y-m-d H:i:00' ),
);

$settings->category = empty( $settings->category ) ?
		array() :
		( is_array( $settings->category ) ? $settings->category : explode( ',', $settings->category ) );

if ( ! empty( $settings->category ) && ! empty( $settings->category[0] ) ) {
	$query['tax_query'] = array(
		array(
			'taxonomy' => 'tribe_events_cat',
			'field'    => 'term_id',
			'terms'    => $settings->category,
		),
	);
}

$events = tribe_get_events( $query );

include locate_template( "templates/beaver/tribe-events/multiple.php" );
